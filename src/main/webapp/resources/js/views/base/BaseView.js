define([
    'underscore',
    'backbone',
], function(
        _,
        Backbone
    ) {

    var BaseView = Backbone.View.extend({
        render : function() {
        }
    })

    return BaseView;
});