define([
    'underscore',
    'backbone',
    'JSOG',
    'bootbox',
    'maskedinput',
    'mods/modUtils',
    'mods/validaciones/modValidator',
    'mods/validaciones/EmpleadoValidator',

    'mods/modCamposDinamicos',
    'collections/ParametrosCollection',
    'models/EmpleadoModel',

    'views/util/SearchEmpleadoView',
    'text!templates/empleado/empleadoTemplate.html',
    ], function(
        _,
        Backbone,
        JSOG,
        bootbox,
        maskedinput,
        modUtils,
        modValidator,
        EmpleadoValidator,

        modCamposDinamicos,
        ParametrosCollection,
        EmpleadoModel,

        SearchEmpleadoView,
        empleadoTemplate
    ) {

    var EmpleadoView = Backbone.View.extend({
        el : '#container',
        initialize : function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.getParametros();
            this.curSearchHotKey = _.bind(this.searchHotKey, this);
            document.addEventListener('keydown', this.curSearchHotKey, false);
        },
        searchHotKey : function(e) {
            var thechar = String.fromCharCode(e.keyCode);
            /* si (alt + p) -> dosearch */
            if (e.altKey && e.keyCode == 80) {
                this.doSearch(e);
            }
        },
        getParametros : function() {
            this.parametros.fetchEmpleadoViewParams({});
        },
        parse2render : function() {
            var params = this.parametros.first();
            this.params = {
                roles : params.get("roles"),
                empleados : JSOG.decode(params.get("empleados"))
            };
            this.render();
        },
        template : _.template(empleadoTemplate),
        render : function(editEmployee) {
            this.editEmployee = editEmployee;
            var self = this;
            this.$el.html(this.template({
                roles : this.params.roles,
                empleado : editEmployee ? editEmployee.toJSON() : null
            }));

            modCamposDinamicos.iniciarChosen({
                selector : '#select-rol'
            });

            modCamposDinamicos.iniciarChosen({
                selector : '#guardiaPersonal'
            });

            modCamposDinamicos.iniciarDatePicker({
                selector : '.datepicker',
                setCurDate : editEmployee ? false : true
            });
            // var maskMoneyParams = {
            //     precision: 0,
            //     defaultZero: false,
            //     thousands: '',
            //     decimal: ''
            // };
            var mask = "9";
            this.$('#nroDocumento').mask("?" + mask.repeat(20), { placeholder : "" } );
            this.$('#celular').mask("?" + mask.repeat(20), { placeholder : "" } );
            this.$('#codigoSap').mask("?" + mask.repeat(8), { placeholder : "" } );
            this.$('#idRh').mask("?" + mask.repeat(6), { placeholder : "" } );
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
            'click .search-empleado' : 'doSearch',
            'blur #nombres' : 'doAutocomplete',
            'blur #paterno' : 'doAutocomplete',
        },
        doAutocomplete : function() {
            var nombres = this.$('#nombres').val();
            var paterno = this.$('#paterno').val();
            var curUsuario = this.$('#usuario').val();
            if (nombres != "" && paterno != "" && curUsuario == "") {
                this.$('#usuario').val((nombres.charAt(0) + paterno.replaceAll(" ", "")).toLowerCase());
                this.$('#correo').val((nombres.substr(0,
                                nombres.indexOf(" ") == -1 ? nombres.length : nombres.indexOf(" ")) + '.' +
                                paterno.replaceAll(" ", "")).toLowerCase() + '@mmg.com');
            }
        },
        doCancel : function(e) {
            e.preventDefault();
            window.location.replace(BASE_URL);
        },
        doSave : function(e) {
            e.preventDefault();
            var self = this;
            var empleadoModel;

            if (this.editEmployee)
                empleadoModel = this.editEmployee;
            else
                empleadoModel = new EmpleadoModel();

            empleadoModel.set({
                nombres : this.$('#nombres').val(),
                paterno : this.$('#paterno').val(),
                materno : this.$('#materno').val(),
                usuario : this.$('#usuario').val(),
                nroDocumento : this.$('#nroDocumento').val(),
                idRol : this.$('#select-rol').val(),
                correo : this.$('#correo').val(),
                celular : this.$('#celular').val() == "" ? null : this.$('#celular').val(),
                fechaIngreso : this.$('#fechaIngreso')
                                        .data('datepicker')
                                        .getFormattedDate(),
                                        // .getFormattedDate('yyyy-mm-dd'),
                codigoSap : this.$('#codigoSap').val() == "" ? null : this.$('#codigoSap').val(),
                idRh : this.$('#idRh').val(),
                guardiaPersonal : this.$('#guardiaPersonal').val()
            });

            var isUpdate = false, msg = "creado";
            var idEmpleado = this.$('#idEmpleado').val();
            if (idEmpleado != '') {
                isUpdate = true;
                empleadoModel.set('idEmpleado', idEmpleado);
                msg = "actualizado";
            }
            else empleadoModel.set('bloqueado', '0');
            var hasMessages = modValidator.validar(
                new EmpleadoValidator(),
                empleadoModel.toJSON()
            )

            var valid = (hasMessages == true) ? hasMessages : false;

            if (!valid) {
                this.$('.save').notify(modValidator.showErrorMessages(hasMessages), {
                    position : 'left top',
                    className : 'error'
                })
            }
            else {
                empleadoModel.on("sync", this.getParametros, this);
                empleadoModel.save({}, {
                    beforeSend : function(jqXHR, settings) {
                        // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        self.editEmployee = null;
                        $.notify('se ha ' + msg + ' el empleado exitosamente', 'success');
                    },
                    error : function(model, response, options) {
                        console.log('response: ', response);
                        $.notify('ERROR conectándose con el servidor', 'error');
                    }
                })
            }
        },
        doSearch : function(e) {
            e.preventDefault();
            /* subview doSearch */
            this.searchView = new SearchEmpleadoView({
                el : '#modal-area',
                empleados : this.params.empleados,
                parent : this,
                showEdit : true
            })
            console.log('se hizo click en doSearch');
        },
        onClose : function() {
            document.removeEventListener('keydown', this.curSearchHotKey, false);
            if (this.searchView)
                this.searchView.close();
        }
    })

    return EmpleadoView;
});