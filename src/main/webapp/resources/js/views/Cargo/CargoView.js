define([
    'underscore',
    'backbone',
    'JSOG',
    'bootbox',
    'mods/modUtils',
    'mods/modCamposDinamicos',
    'collections/ParametrosCollection',
    'models/CargoModel',
    'views/DependenciaOrganizacional/subviews/HierachyDepView',
    'views/util/SearchEmpleadoView',
    // 'collections/CargoCollection',
    'text!templates/cargo/cargoTemplate.html',
    'text!templates/cargo/cargoShowPerfilTemplate.html',
    'text!templates/cargo/css/cargo.css',
    ], function(
        _,
        Backbone,
        JSOG,
        bootbox,
        modUtils,
        modCamposDinamicos,
        ParametrosCollection,
        CargoModel,
        // CargoCollection,
        HierachyDepView,
        SearchEmpleadoView,
        cargoTemplate,
        cargoShowPerfilTemplate,
        cargocss
    ) {

    var CargoView = Backbone.View.extend({
        el : '#container',
        initialize : function(options) {
            $('#custom-css').empty();
            $('#custom-css').html(cargocss);
            modUtils.extractToContext(options, this);
            /* parameter : nivel */
            /* parameter : idDependencia */

            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.getParametros();
            this.curSearchHotKey = _.bind(this.searchHotKey, this);
            document.addEventListener('keydown', this.curSearchHotKey, false);
        },
        searchHotKey : function(e) {
            var thechar = String.fromCharCode(e.keyCode);
            /* si -> alt + p */
            if (e.altKey && e.keyCode == 80) {
                this.doSearch(e);
            }
        },
        getParametros : function() {
            this.parametros.fetchCargoViewParams({
                nivel : this.nivel,
                idDependencia : this.idDependencia,
            })
        },

        parse2render : function() {
            var params = this.parametros.first();
            var cargos = JSOG.decode(params.get("cargos"));
            this.params = {
                hierachyDep : JSOG.decode(params.get("hierachyDep")).reverse(),
                empleados : params.get("empleados"),
                groupCargos : modUtils.toGroupList(cargos, 4),
                cargos : cargos,
                totalCargos : cargos.length,
                nivelesJerarquicos : JSOG.decode(params.get("nivelesJerarquicos"))
            }
            this.render();
            /* after render (load sub views) */
            /* subview hierachyDep */
            this.hierachyDepView = new HierachyDepView({
                el : '#hierachy-dep',
                nivel : this.nivel,
                hierachyDep : this.params.hierachyDep,
                nivelesJerarquicos : this.params.nivelesJerarquicos
            })
        },
        template : _.template(cargoTemplate),
        render : function() {
            var self = this;
            console.log('this.params.cargos: ', this.params.cargos);
            this.$el.html(this.template({
                modUtils : modUtils,
                empleados : this.params.empleados,
                groupCargos : this.params.groupCargos,
                idEmpleado : '',
                cargos : this.params.cargos,
                totalCargos : this.params.totalCargos,
                idCargo : '',
            }));

            modCamposDinamicos.iniciarChosen({
                selector : '#select-cargo',
                placeholder : 'buscar cargo, si no existe añádalo manualmente debajo'
            })
            modCamposDinamicos.iniciarChosen({
                selector : '#select-empleado',
                placeholder : 'elegir empleado a asignar cargo'
            })


            this.$('#select-cargo').change(function() {
                var idCargo = $(this).val();
                var curCargo  = _.find(self.params.cargos, function(cargo) {
                    return cargo.idCargo == idCargo;
                });

                // self.$('#idCargo').val(idCargo);
                self.$('#descripcion-cargo').val(curCargo.descripcionCargo);
                self.$('#abreviatura-cargo').val(curCargo.abreviaturaCargo);
            })

            this.$('#select-empleado').change(function() {
                var idEmpleado = $(this).val();
                var curEmpleado  = _.find(self.params.empleados, function(empleado) {
                    return empleado.idEmpleado == idEmpleado;
                });
                self.$('#idEmpleado').val(idEmpleado);
                // console.log('empleado seleccionado:');
                // console.log('curEmpleado: ', curEmpleado);
            })

        },
        toggle : function(selector) {
            /* toggleamos entre vistas (una se esconde la otra se muestra) */
            this.$('#people-list').hide();
            this.$('#save-cargo').hide();
            this.$('#show-profile-cargo').hide();
            this.$('#show-profile').hide();
            this.$(selector).show();
        },
        events : {
            'click .add-new-cargo' : 'addNewCargo',
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
            'click .remove-cargo' : 'doDelete',
            'click .show-profile-cargo' : 'doShowProfileCargo',
            'click .search-cargo' : 'doSearch',
        },
        addNewCargo : function(e) {
            e.preventDefault();
            var self = this;
            this.$('#idCargo').val('');
            this.$('#idEmpleado').val('');
            this.$('#descripcion-cargo').val('');
            this.$('#abreviatura-cargo').val('');
            setTimeout(function() {
                self.$('#select-cargo').trigger('chosen:activate');
            }, 100);
            this.toggle('#save-cargo');
        },
        doCancel : function(e) {
            e.preventDefault();
            this.toggle('#people-list');
        },
        doSave : function(e) {
            e.preventDefault();
            var self = this;
            var cargoModel = new CargoModel({
                descripcionCargo : this.$('#descripcion-cargo').val(),
                abreviaturaCargo : this.$('#abreviatura-cargo').val(),
                dependenciaOrganizacional : {
                    idDependenciaOrganizacional : this.idDependencia
                }
            });
            var idEmpleado = parseInt(this.$('#idEmpleado').val());
            if (idEmpleado)
                cargoModel.set('empleado', { idEmpleado : idEmpleado })

            var idCargo = this.$('#idCargo').val();
            var isUpdate = false, msg = "creado";
            if (idCargo != '') {
                isUpdate = true;
                msg = "actualizado";
                cargoModel.set('idCargo', idCargo);
            }
            cargoModel.on("sync", this.getParametros, this);
            cargoModel.save({}, {
                beforeSend : function(jqXHR, settings) {
                    // console.log('settings.data: ', settings.data);
                },
                success : function(model, response, options) {
                    $.notify('se ha ' + msg + ' el cargo exitosamente', 'success');
                },
                error : function(model, response, options) {
                    console.log('response: ', response);
                    if (self.params.asignar)
                        $.notify('ERROR el empleado ya tiene un cargo asignado', 'error');
                    else
                        $.notify('ERROR conectándose con el servidor', 'error');
                }
            })
        },
        doDelete : function (e) {
            var idCargo = $(e.currentTarget).attr('data-cargo-id');
            e.preventDefault();

            var cargoModel = new CargoModel({
                idCargo : idCargo
            });
            cargoModel.on("sync", this.getParametros, this);
            bootbox.confirm('<h4 class="text-center">' +
                    '¿ESTÁS SEGURO DE QUITAR ESTE CARGO?<strong></h4>',
            function(ok) {
                if (!ok) return;
                cargoModel.destroy({
                    beforeSend : function(jqXHR, settings) {
                        // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        $.notify('se eliminó exitosamente el cargo ', 'success');
                    },
                    error : function(model, response, options) {
                        console.log('response: ', response);
                        $.notify('ERROR conectándose con el servidor', 'error');
                    }
                });
            })
        },
        doShowProfileCargo : function(e) {
            e.preventDefault();
            var self = this;
            var idCargo = $(e.currentTarget).attr('data-cargo-id');
            var cargo =  _.find(this.params.cargos, function(cargo) {
                return cargo.idCargo == idCargo;
            });

            this.$('#descripcion-cargo-static').html(cargo.descripcionCargo);
            this.$('#abreviatura-cargo-static').html(cargo.abreviaturaCargo);
            this.$('#descripcion-cargo').val(cargo.descripcionCargo);
            this.$('#abreviatura-cargo').val(cargo.abreviaturaCargo);

            // console.log('cargo: ', cargo);

            this.$('#select-empleado')
            .val(cargo.empleado != null ? cargo.empleado.idEmpleado : "")
            .trigger('chosen:updated');
            /* el foco no aparece automáticamente */
            setTimeout(function() {
                self.$('#select-empleado').trigger('chosen:activate');
            }, 100);

            this.params.asignar = true;
            this.$('#idCargo').val(idCargo);

            if (cargo.empleado != null) {
                this.$('[href=#change-cargo-tab]').closest('li').removeClass('active');
                this.$('#change-cargo-tab').removeClass('active');

                this.$('[href=#show-profile-tab]').closest('li').addClass('active');
                this.$('#show-profile-tab').addClass('active');
                this.$('#show-profile-tab').html(
                    _.template(
                    cargoShowPerfilTemplate, {
                        empleado : cargo.empleado,
                        modUtils : modUtils
                    })
                )
            } else {
                this.$('#show-profile-tab').empty();
                this.$('[href=#show-profile-tab]').closest('li').removeClass('active');
                this.$('#show-profile-tab').removeClass('active');

                this.$('[href=#change-cargo-tab]').closest('li').addClass('active');
                this.$('#change-cargo-tab').addClass('active');
            }

            this.toggle('#show-profile-cargo');
        },
        doSearch : function(e) {
            e.preventDefault();
            /* subview doSearch */
            this.searchView = new SearchEmpleadoView({
                el : '#modal-area',
                empleados : this.params.empleados,
                showEdit : false
            })
        },
        onClose : function() {
            document.removeEventListener('keydown', this.curSearchHotKey, false);
            this.hierachyDepView.close();
            if (this.searchView)
                this.searchView.close();
        }
    })

    return CargoView;
});