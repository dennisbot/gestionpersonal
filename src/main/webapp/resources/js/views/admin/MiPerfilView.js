define([
    'underscore',
    'backbone',

    'models/EmpleadoModel',
    'mods/modAjax',
    'mods/modUtils',
    'bootbox',
    'JSOG',

    'text!templates/admin/miPerfilTemplate.html',
    'text!templates/admin/css/miPerfil.css',
    'mods/modCamposDinamicos',

], function(
        _,
        Backbone,

        EmpleadoModel,
        modAjax,
        modUtils,
        bootbox,
        JSOG,

        miPerfilTemplate,
        miPerfilcss,
        modCamposDinamicos
    ) {

    var MiPerfilView = Backbone.View.extend({
        el : '#container',
        initialize : function() {
            console.log('si se inicializó MiPerfilView');
            $('#custom-css').empty();
            /* set the custom css */
            $('#custom-css').html(miPerfilcss);
        },
        events : {
            'click #edit-profile' : 'doShowEditProfile',
            'click #change-password' : 'doShowPanelChangePassword',
            'click .btn-change-password' : 'doChangePassword',
        },
        iniciarUploader : function() {
            modCamposDinamicos.iniciarUploaderFoto({
                selector: '#cambiar-cargar-imagen',
                endpoint: 'uploadFoto/' + parametros.usuario.idEmpleado,
                text: {
                    uploadButton:
                    '<i class="fa fa-upload fa-lg" style="margin-right:15px">' +
                    '</i> Subir/cambiar foto de perfil'
                },
                // showGrid : _.bind(this.showGrid, this),
                // context : self
            });
        },
        doChangePassword : function(e) {
            e.preventDefault();
            var claveAntigua, claveNueva, repiteClaveNueva;
            claveAntigua = this.$('#clave-antigua').val();
            claveNueva = this.$('#clave-nueva').val();
            repiteClaveNueva = this.$('#repite-clave-nueva').val();
            // console.log('claveNueva: ', claveNueva);
            // console.log('repiteClaveNueva: ', repiteClaveNueva);
            if (claveNueva != repiteClaveNueva) {
                bootbox.alert('<h4 class="text-center"><strong>' +
                    'La nueva clave no coindice con el que repite</strong></h4>');
                return false;
            }
            modAjax.callAJAXPostJSON({
                url: 'rest/empleado/changePassword',
                jsonVars : {
                    empleado : parametros.usuario,
                    oldPassword : claveAntigua,
                    newPassword : claveNueva
                },
                // jsonVars : parametros.usuario,
                callback : function(response) {
                    console.log('OK!!');
                    console.log('response: ', response);
                    bootbox.alert('<h4 class="text-center"><strong>' + response.message + '</strong></h4>')
                },
                onerror : function(response) {
                    console.log('ERROR!!');
                    console.log('response: ', response);
                    bootbox.alert('<h4 class="text-center"><strong>' + response.message + '</strong></h4>')
                }
            })
            console.log('do change password');
        },
        showChangePassword : function() {
            this.$('#panel-perfil').hide();
            this.$('#panel-cambiar-clave').show();
        },
        showEditProfile : function() {
            this.$('#panel-perfil').show();
            this.$('#panel-cambiar-clave').hide();
        },
        doShowPanelChangePassword : function(e) {
            e.preventDefault();
            this.showChangePassword();
            this.$('#clave-antigua').select();
        },
        doShowEditProfile : function(e) {
            e.preventDefault();
            this.showEditProfile();
            var self = this;
            $('.edit-in-place').animate({
                backgroundColor : '#ff9'
            }, 300, function() {
                $(this).animate({backgroundColor : 'transparent'}, 2000);
            })
            console.log('se hizo click en edit profile');
            var options = {
                selector : '.edit-in-place',
                args : {
                    error_sink : function(idOfEditor, errorString) {
                        console.log('hubo un error: ', errorString);
                    },
                    callback : function(element_id, update_value, original_value) {
                        if (original_value.trim() == update_value.trim()) return original_value;
                        var html_response = original_value;

                        var curCell = $('#' + element_id);
                        var idEmpleado = parametros.usuario.idEmpleado;
                        var empleadoModel = new EmpleadoModel();
                        empleadoModel.set('idEmpleado', idEmpleado);
                        empleadoModel.fetch({
                            async: false,
                            beforeSend : function(jqXHR, settings) {
                                console.log('settings.data: ', settings.data);
                                $('.' + element_id).show();
                            },
                            success: function(model, response, options) {
                                /* element_id es el nombre de la propiedad */
                                empleadoModel.set(element_id, update_value);
                                /* luego procedemos a hacer la actualización */
                                empleadoModel.saveProfile({}, {
                                    async: false,
                                    success: function(model, response, options) {
                                        $('.' + element_id).hide();
                                        if (element_id == 'nombres' || element_id == 'paterno') {
                                            var toDisplay = empleadoModel.get('nombres');
                                            toDisplay += ' ' + empleadoModel.get('paterno') ;
                                            toDisplay = modUtils.toTitleCase(toDisplay);
                                            $('#nombre-para-mostrar').html(toDisplay);
                                            $('#usuario-perfil').html(toDisplay);
                                            $('#usuario-sidebar').html(toDisplay);
                                            $('#usuario-small').html(toDisplay);
                                        }
                                        /* cambiamos los valores de paramametros */
                                        parametros.usuario = empleadoModel.toJSON();
                                        // console.log('JSOG.decode(empleadoModel.toJSON()): ',
                                        //     JSOG.decode(empleadoModel.toJSON()));
                                        $(curCell).notify('guardado!', {
                                            position : 'right bottom',
                                            className : 'success'
                                        });
                                        html_response = update_value;
                                    },
                                    error : function(model, response, options) {
                                        console.log('error guardando el empleado, detalles del error:');
                                        console.log('response: ', response);
                                        $.notify('ERROR conectandose con el servidor', 'error');
                                    }
                                })
                            },
                            error : function(model, response, options) {
                                console.log('response: ', response);
                                $.notify('ERROR conectandose con el servidor', 'error');
                            }
                        })

                        return html_response;
                    },
                    default_text : '-',
                    use_bg_out : true,
                    hover_class : 'edit-in-place-hover',
                    value_required : true,
                },
                inputInplaceFieldSelector : '#container input.inplace_field',
                onfocus : function() {
                    if (curOuterWidth)
                        self.curOuterWidth = curOuterWidth;
                    if (curOuterHeight)
                        self.curOuterHeight = curOuterHeight;
                    $(this).closest('p').css({
                        paddingTop: "0px",
                        paddingBottom: "1px"
                    });
                    $(this).closest('form').css({marginLeft : '-5px'});
                    $(this).css({
                        'height': self.curOuterHeight - 1,
                        'width': self.curOuterWidth + 4,
                        'text-align': 'left',
                        'border-style': 'none',
                        'border-width': '0',
                        'padding': '5px',
                        'margin': '0',
                    });
                },
                onblur : function() {
                    $(this).closest('p').css({
                        paddingTop: "7px",
                        paddingBottom: "7px"
                    })
                }
            }
            modCamposDinamicos.iniciarEditInPlace(options);
        },
        template : _.template(miPerfilTemplate),
        render : function() {
            var tpl = this.template({
                modUtils : modUtils
            });
            this.$el.html(tpl);
            this.iniciarUploader();
        }
    })

    return MiPerfilView;
});