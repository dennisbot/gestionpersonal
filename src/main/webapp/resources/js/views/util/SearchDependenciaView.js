define([
    'underscore',
    'backbone',
    'mods/modUtils',
    'mods/DataTable/DataTable',
    'config/searchDependenciaConfig',
    'text!templates/util/searchDependenciaTemplate.html'
    ],
function(
        _,
        Backbone,
        modUtils,
        DataTable,
        searchDependenciaConfig,
        searchDependenciaTemplate
) {

    var SearchEmpleadoView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.descripcionesDeps = _.pluck(this.dependencias, 'descripcionDependencia');
            // console.log('this.descripcionesDeps: ', this.descripcionesDeps);
            this.render();
        },
        events : {
            'keyup #search-term' : 'doSearch'
        },
        template : _.template(searchDependenciaTemplate),
        /* fuzzy, mejor implementación */
        render : function() {
            var self = this;
            this.$el.html(this.template({}));
            this.$('#myModal').modal().on('shown.bs.modal', function() {
                self.$('#search-term').focus();
                self.dt = new DataTable();
                console.log('se ha mostrado el modal');
            })
        },
        doSelectSearch : function(e, curItem) {
            this.$('#myModal').modal('hide');
        },
        doSearch : function(e) {
            var self = this;
            var options = {
                pre: '<span class="highlight">',
                post: '</span>'
            };
            if (e.altKey || e.ctrlKey || e.shiftKey || e.keyCode == 18 ||
                e.keyCode == 17 || e.keyCode == 13 || e.keyCode == 16
                || e.keyCode == 20 || e.keyCode == 27 || e.keyCode == 9) return false;

            var searchString = $(e.currentTarget).val();
            /* vamos a limpiar el contenido */
            if (searchString.length == 0 && this.dt.datatable) {
                this.dt.datatable.clear();
                this.dt.datatable.columns.adjust().draw();
            }
            /* nos aseguramos que al menos sea longitud de 2 */
            if (searchString.length < 2) return false;

            $('#div-table-search-container').show();
            var results = fuzzy.filter(searchString, self.descripcionesDeps, options);
            var newCollection = _.map(results, function(result) {
                self.dependencias[result.index].descripcionItemResult = result.string;
                return self.dependencias[result.index];
            });
            if (this.dt.datatable) {
                this.dt.datatable.clear();
                if (newCollection.length > 0) {
                    this.dt.datatable.rows.add(newCollection);
                }
                this.dt.datatable.columns.adjust().draw();
                this.dt.prepararEdiciones();
            }
            else {
                this.dt
                    .showGrid(newCollection, searchDependenciaConfig.getConfig(self));
            }
        },
        onClose : function() {
            this.dt = null;
        }
    })

    return SearchEmpleadoView;
});