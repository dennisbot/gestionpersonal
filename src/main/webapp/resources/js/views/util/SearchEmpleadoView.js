define([
    'underscore',
    'backbone',
    'JSOG',
    'models/EmpleadoModel',
    'mods/modUtils',
    'mods/DataTable/DataTable',
    'config/searchEmpleadoConfig',
    'text!templates/util/searchEmpleadoTemplate.html'
    ],
function(
        _,
        Backbone,
        JSOG,
        EmpleadoModel,
        modUtils,
        DataTable,
        searchEmpleadoConfig,
        searchEmpleadoTemplate
) {

    var SearchEmpleadoView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.nombresParaMostrar = _.pluck(this.empleados, 'nombreParaMostrar');
            // console.log('this.nombresParaMostrar: ', this.nombresParaMostrar);
            // console.log('this.empleados: ', this.empleados);
            this.render();
        },
        events : {
            'keyup #search-term' : 'doSearch'
        },
        template : _.template(searchEmpleadoTemplate),
        /* fuzzy, mejor implementación */
        render : function() {
            var self = this;
            this.$el.html(this.template({}));
            this.$('#myModal').modal().on('shown.bs.modal', function() {
                self.$('#search-term').focus();
                self.dt = new DataTable();
            })
        },
        doSelectSearch : function(e, curItem) {
            this.$('#myModal').modal('hide');
        },
        doEdit : function(curItem) {
            this.empleadoModel = new EmpleadoModel({
                idEmpleado : curItem.idEmpleado
            })
            this.empleadoModel.on('sync', this.callParentRender, this);
            this.empleadoModel.fetch();
            // console.log('curItem: ', curItem);
        },
        callParentRender : function() {
            this.doSelectSearch();
            this.parent.render(this.empleadoModel);
        },
        doSearch : function(e) {
            var self = this;
            var options = {
                pre: '<span class="highlight">',
                post: '</span>'
            };
            if (e.altKey || e.ctrlKey || e.shiftKey || e.keyCode == 18 ||
                e.keyCode == 17 || e.keyCode == 13 || e.keyCode == 16
                || e.keyCode == 20 || e.keyCode == 27 || e.keyCode == 9) return false;

            var searchString = $(e.currentTarget).val();
            /* vamos a limpiar el contenido */
            if (searchString.length == 0 && this.dt.datatable) {
                this.dt.datatable.clear();
                this.dt.datatable.columns.adjust().draw();
            }
            /* nos aseguramos que al menos sea longitud de 3 */
            if (searchString.length < 3) return false;

            $('#div-table-search-container').show();
            var results = fuzzy.filter(searchString, self.nombresParaMostrar, options);
            var newCollection = _.map(results, function(result) {
                self.empleados[result.index].descripcionItemResult = result.string;
                return self.empleados[result.index];
            });
            if (this.dt.datatable) {
                this.dt.datatable.clear();
                if (newCollection.length > 0) {
                    this.dt.datatable.rows.add(newCollection);
                }
                this.dt.datatable.columns.adjust().draw();
                this.dt.prepararEdiciones();
            }
            else {
                this.dt
                    .showGrid(newCollection, searchEmpleadoConfig.getConfig(self));
            }
        },
        onClose : function() {
            this.dt = null;
        }
    })

    return SearchEmpleadoView;
});