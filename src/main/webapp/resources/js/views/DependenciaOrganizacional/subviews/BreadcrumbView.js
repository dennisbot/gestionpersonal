define([
    'underscore',
    'backbone',
    'mods/modUtils',
    'text!templates/dependenciaOrganizacional/subviews/breadcrumbTemplate.html'
], function(
    _,
    Backbone,
    modUtils,
    breadcrumbTemplate
) {

    var BreadcrumbView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            /* parameter : nivel */
            /* parameter : nivelesJerarquicos */
            this.render();
        },
        template : _.template(breadcrumbTemplate),
        render : function() {
            this.$el.html(this.template({
                nivel : this.nivel,
                nivelesJerarquicos : this.nivelesJerarquicos
            }));
        }
    })

    return BreadcrumbView;
});