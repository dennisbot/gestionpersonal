define([
    'underscore',
    'backbone',
    'mods/modUtils',
    'text!templates/dependenciaOrganizacional/subviews/hierachyDepTemplate.html'
], function(
    _,
    Backbone,
    modUtils,
    hierachyDepTemplate
) {

    var BreadcrumbView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            /* parameter : nivel */
            /* parameter : hierachyDep */
            /* parameter : nivelesJerarquicos */
            this.render();
        },
        template : _.template(hierachyDepTemplate),
        render : function() {
            this.$el.html(this.template({
                nivel : this.nivel,
                hierachyDep : this.hierachyDep,
                nivelesJerarquicos : this.nivelesJerarquicos,
            }));
        }
    })

    return BreadcrumbView;
});