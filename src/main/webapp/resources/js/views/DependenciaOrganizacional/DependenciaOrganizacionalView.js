define([
        'underscore',
        'backbone',
        'JSOG',
        'bootbox',
        'maskedinput',
        'mods/validaciones/modValidator',
        'mods/validaciones/DependenciaOrganizacionalValidator',
        'mods/modCamposDinamicos',
        'mods/modDependencia',
        'collections/DependenciaOrganizacionalCollection',
        'collections/NivelesJerarquicosCollection',
        'models/NivelJerarquicoModel',
        'models/DependenciaOrganizacionalModel',
        'mods/modUtils',
        'mods/DataTable/DataTable',

        'collections/ParametrosCollection',

        'config/dependenciaOrganizacionalConfig',

        'views/DependenciaOrganizacional/subviews/BreadcrumbView',
        'views/DependenciaOrganizacional/subviews/HierachyDepView',

        'views/util/SearchDependenciaView',
        'text!templates/dependenciaOrganizacional/dependenciaOrganizacionalTemplate.html',
        'text!templates/dependenciaOrganizacional/selectDependenciaParentTemplate.html',
    ], function(
        _,
        Backbone,
        JSOG,
        bootbox,
        maskedinput,
        modValidator,
        DependenciaOrganizacionalValidator,
        modCamposDinamicos,
        modDependencia,
        DependenciaOrganizacionalCollection,
        NivelesJerarquicosCollection,
        NivelJerarquicoModel,
        DependenciaOrganizacionalModel,
        modUtils,
        DataTable,

        ParametrosCollection,

        dependenciaOrganizacionalConfig,

        BreadcrumbView,
        HierachyDepView,

        SearchDependenciaView,
        dependenciaOrganizacionalTemplate,
        selectDependenciaParentTemplate
    ) {

    var DependenciaOrganizacionalView = Backbone.View.extend({
        el : '#container',
        initialize : function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            /* parameter : nivel */
            /* parameter : idDependencia */
            // console.log('this.nivel: ', this.nivel);
            // console.log('this.idDependencia: ', this.idDependencia);
            this.dependencia = modDependencia.getInstance(this.nivel);
            // console.log('this.dependencia: ', this.dependencia);
            // this.nivelesJerarquicos = new NivelesJerarquicosCollection();
            // this.nivelesJerarquicos.on('sync', this.render, this);
            // this.nivelesJerarquicos.fetchLessThanEqualNivel({ nivel : this.nivel });
            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.getParametros();
            this.curSearchHotKey = _.bind(this.searchHotKey, this);
            document.addEventListener('keydown', this.curSearchHotKey, false);
        },
        searchHotKey : function(e) {
            var thechar = String.fromCharCode(e.keyCode);
            /* si (alt + p) -> dosearch */
            if (e.altKey && e.keyCode == 80) {
                this.doSearch(e);
            }
        },
        getParametros : function() {
            this.parametros.fetchDependenciaOrganizacionalViewParams({
                nivel : this.nivel,
                idDependencia : this.idDependencia ? this.idDependencia : 0
            });
        },
        canAddNewDependencia : function() {
            return this.idDependencia;
        },
        getCurNivelJerarquico : function() {
            return this.params.nivelesJerarquicos[this.params.hierachyDep.length];
        },
        parse2render : function() {
            var params = this.parametros.first();
            this.params = {
                hierachyDep : JSOG.decode(params.get("hierachyDep")).reverse(),
                allNivelesJerarquicos : JSOG.decode(params.get("allNivelesJerarquicos")),
                nivelesJerarquicos : JSOG.decode(params.get("nivelesJerarquicos")),
                dependenciasOrgsLevel : JSOG.decode(params.get("dependenciasOrgsLevel")),
                dependencias : JSOG.decode(params.get("dependencias")),
                oneLevelBackDeps : JSOG.decode(params.get("oneLevelBackDeps")),
            }
            this.render();
            /* after render (load sub views) */
            /* subview breadcrumb */
            this.breadcrumbView = new BreadcrumbView({
                el : '#breadcrumb-container',
                nivel : this.nivel,
                nivelesJerarquicos : this.params.nivelesJerarquicos,
            });
            /* subview hierachyDep */
            this.hierachyDepView = new HierachyDepView({
                el : '#hierachy-dep',
                nivel : this.nivel,
                hierachyDep : this.params.hierachyDep,
                nivelesJerarquicos : this.params.nivelesJerarquicos
            })
        },
        getNextCodDependencia : function() {
            var nextCodDependencia;
            dependenciaOrganizacionalModel = new DependenciaOrganizacionalModel();
            dependenciaOrganizacionalModel.getNextCodDependencia({
                async : false,
                success: function(model, response, options) {
                    nextCodDependencia = response;
                },
                error: function(model, response) {
                    console.log('response: ', response);
                }
            });
            return nextCodDependencia;
        },
        template : _.template(dependenciaOrganizacionalTemplate),
        render : function() {
            // console.log('this.params.hierachyDep: ', this.params.hierachyDep);
            // console.log('this.params.dependenciasOrgsLevel: ', this.params.dependenciasOrgsLevel);
            // console.log('this.params.nivelesJerarquicos: ', this.params.nivelesJerarquicos);
            modUtils.processCustomMessages();
            this.$el.html(this.template({
                 oneLevelBackDeps : this.params.oneLevelBackDeps,
                 descripcionHeaderNivelDependencia : ' ' + this.dependencia
                    .getDescripcionHeaderNivelDependencia(
                        this.getCurNivelJerarquico()
                    ),
                allNivelesJerarquicos : this.params.allNivelesJerarquicos,
                nivel : this.nivel > this.getCurNivelJerarquico().nivel
                        ? this.nivel : this.getCurNivelJerarquico().nivel
                // nivel : this.getCurNivelJerarquico().nivel
            }));
            this.$('#cod-dependencia').mask('DEP-999', {placeholder : "_"})
            modCamposDinamicos.iniciarChosen({
                selector : '#select-dependencia-parent',
            })
            modCamposDinamicos.iniciarChosen({
                selector : '#select-nivel-jerarquico',
            })

            /* mostramos la tabla */
            this.dataTable = new DataTable();
            var config = dependenciaOrganizacionalConfig.getConfig(this);
            this.dataTable.showGrid(this.params.dependenciasOrgsLevel, config);
            if (this.canAddNewDependencia())
                this.$('.add-item').html(this.dependencia.getNewDependenciaButton(
                    this.getCurNivelJerarquico()
                ));
        },
        toggle : function(item) {
            var self = this;
            /* toggleamos entre vistas (una se esconde la otra se muestra) */
            this.$('#move-dependencia').hide();
            this.$('#save-dependencia').hide();
            this.$('#select-table-dependencia-organizacional_wrapper').hide();
            this.$(this.panelSelector).show();

            if (item) {
                // console.log('item: ', item);
                this.$('#idDependenciaOrganizacional').val(item.idDependenciaOrganizacional);
                this.$('#cod-dependencia').val(item.codDependencia);
                this.$('#select-nivel-jerarquico').val(item.nivelJerarquico.idNivelJerarquico)
                .trigger('chosen:updated');
                this.$('#descripcion-dependencia').val(item.descripcionDependencia);
                this.$('#abreviatura-dependencia').val(item.abreviaturaDependencia);
            }
            else {
                this.curItem = null;
                this.$('#idDependenciaOrganizacional').val('');
                this.$('#cod-dependencia').val(this.getNextCodDependencia());
                this.$('#descripcion-dependencia').val('');
                this.$('#abreviatura-dependencia').val('');
            }
            // this.$('#cod-dependencia').focus().select();
            self.$('#select-nivel-jerarquico').trigger('chosen:activate');
        },
        events : {
            'click .add-new-item' : 'doSaveItem',
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
            'click .move' : 'doSave',
            'change #select-dependencia-parent' : 'doChangeIdDependencia',
            'click .search-dependencia' : 'doSearch',
        },
        doChangeIdDependencia : function(e) {
            this.newIdDependencia = $(e.currentTarget).val();
        },
        doSaveItem : function() {
            this.$('#dep-action-type').html('Crear');
            this.panelSelector = '#save-dependencia';
            this.toggle(null);
        },
        doCancel : function(e) {
            e.preventDefault();
            this.panelSelector = '#select-table-dependencia-organizacional_wrapper';
            this.toggle(null);
            this.curItem = null;
        },
        doSave : function(e) {
            var self = this;
            e.preventDefault();
            var curButton = e.currentTarget;
            this.$(curButton).addClass('disabled');
            var dependenciaOrganizacionalModel;
            /* si es this.curItem es move */
            if (this.curItem) {
                /* inicializamos la dep org con el curItem (del doMove) */
                dependenciaOrganizacionalModel = new DependenciaOrganizacionalModel(this.curItem);
            }
            else
                dependenciaOrganizacionalModel = new DependenciaOrganizacionalModel({
                    codDependencia : this.$('#cod-dependencia').val(),
                    descripcionDependencia : this.$('#descripcion-dependencia').val(),
                    abreviaturaDependencia : this.$('#abreviatura-dependencia').val(),
                    nivelJerarquico : {
                        idNivelJerarquico : this.$('#select-nivel-jerarquico').val()
                        // idNivelJerarquico : this.getCurNivelJerarquico().idNivelJerarquico
                    }
                });
            if (dependenciaOrganizacionalModel.get('codDependencia') == '')
                dependenciaOrganizacionalModel.set('codDependencia', null);

            if (this.idDependencia) {
                dependenciaOrganizacionalModel.set('dependenciaOrganizacional', {
                    idDependenciaOrganizacional : this.idDependencia
                })
            }
            if (this.newIdDependencia) {
                dependenciaOrganizacionalModel.set('dependenciaOrganizacional', {
                    idDependenciaOrganizacional : this.newIdDependencia
                })
            }

            var idDependenciaOrganizacional = this.$('#idDependenciaOrganizacional').val();
            var isUpdate = false, msg = "creado";
            if (idDependenciaOrganizacional != '') {
                isUpdate = true;
                msg = "actualizado";
                dependenciaOrganizacionalModel.set('idDependenciaOrganizacional', idDependenciaOrganizacional);
                if (this.newIdDependencia)
                    msg = "movido";
            }

            var hasMessages = modValidator.validar(
                new DependenciaOrganizacionalValidator(),
                dependenciaOrganizacionalModel.toJSON()
            )

            var valid = (hasMessages == true) ? hasMessages : false;
            // console.log('valid: ', valid);
            if (!valid) {
                var buttonClassSelectorSave = (this.panelSelector == '#move-dependencia') ? '.move' : '.save';

                this.$(buttonClassSelectorSave).notify(modValidator.showErrorMessages(hasMessages), {
                    position : 'left top',
                    className : 'error'
                })
            }
            else {
                dependenciaOrganizacionalModel.on("sync", this.getParametros, this);
                dependenciaOrganizacionalModel.save({}, {
                    beforeSend : function(jqXHR, settings) {
                        // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        $.notify('se ha ' + msg + ' la dependencia exitosamente', 'success');
                    },
                    error : function(model, response, options) {
                        console.log('response: ', response);
                        $.notify('ERROR conectándose con el servidor', 'error');
                    },
                    complete : function() {
                        self.$(curButton).removeClass('disabled');
                        self.curItem = null;
                    }
                })
            }
        },
        /* estos métodos se llaman desde el datatable */
        doEdit : function(curItem) {
            this.$('#dep-action-type').html('Editar');
            this.panelSelector = '#save-dependencia';
            this.toggle(curItem);
        },
        doMove : function(curItem) {
            this.curItem = curItem;
            this.panelSelector = '#move-dependencia';
            this.toggle(curItem);
            console.log('this.curItem: ', this.curItem);
            this.depsCollection = new DependenciaOrganizacionalCollection();
            this.depsCollection.on('sync', function() {
                if (curItem.dependenciaOrganizacional) {
                    var compiledTemplate = _.template(selectDependenciaParentTemplate, {
                        depsBetweenLevels : JSOG.decode(this.depsCollection.toJSON()),
                        idDependenciaOrganizacional : curItem.dependenciaOrganizacional.idDependenciaOrganizacional
                    });
                    this.$('#select-dependencia-parent').html(compiledTemplate);
                    modCamposDinamicos.iniciarChosen({
                        selector : '#select-dependencia-parent',
                    })
                    // this.$('#select-dependencia-parent')
                    // .val(curItem.dependenciaOrganizacional.idDependenciaOrganizacional)
                    // .trigger('chosen:updated')
                    // .trigger('chosen:activate');
                }
            }, this);
            this.depsCollection.fetchDepsBetweenLevels({
                loLvl : this.nivel - 1,
                hiLvl : curItem.nivelJerarquico.idNivelJerarquico - 1
            });
        },
        doDelete : function(curItem) {
            // console.log('curItem: ', curItem);
            var dependenciaOrganizacionalModel = new DependenciaOrganizacionalModel({
                idDependenciaOrganizacional : curItem.idDependenciaOrganizacional
            });
            dependenciaOrganizacionalModel.on("sync", this.getParametros, this);
            bootbox.confirm('<h4 class="text-center">' +
                    '¿ESTÁS SEGURO DE QUITAR LA DEPENDENCIA <strong>' +
                    curItem.descripcionDependencia +
                    '?</strong></h4>',
            function(ok) {
                if (!ok) return;
                dependenciaOrganizacionalModel.destroy({
                    beforeSend : function(jqXHR, settings) {
                        // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        $.notify('se eliminó exitosamente la dependencia', 'success');
                    },
                    error : function(model, response, options) {
                        console.log('response: ', response);
                        bootbox.alert('<h3 class="text-center">no se ha podido completar' +
                            ' la operación, primero debe eliminar las dependencias hijo ' +
                            ' con sus respectivos cargos</h4>');
                        // $.notify('ERROR conectándose con el servidor', 'error');
                    }
                });
            })
        },
        doSearch : function(e) {
            e.preventDefault();
            /* subview doSearch */
            this.searchView = new SearchDependenciaView({
                el : '#modal-area',
                dependencias : this.params.dependencias
            })
        },
        onClose : function() {
            document.removeEventListener('keydown', this.curSearchHotKey, false);
            this.breadcrumbView.close();
            this.hierachyDepView.close();
            if (this.searchView)
                this.searchView.close();
        }
    })

    return DependenciaOrganizacionalView;
});