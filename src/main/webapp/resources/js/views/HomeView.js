define(
    [
        'underscore',
        'backbone',

        'mods/modUtils',

        'text!templates/homeTemplate.html',
        // 'JSOG',
    ]
    , function(
        _,
        Backbone,

        modUtils,

        homeTemplate
        // ,JSOG
    ) {
    var HomeView = Backbone.View.extend({
        el: '#container',
        initialize: function() {
            $('#custom-css').empty();
            this.render();
            // console.log('JSOG: ', JSOG);
            // $.ajax({
            //     url: 'http://localhost:8080/GestionPersonal/rest/nivelJerarquico/1',
            //     success: function(response) {
            //         console.log('response: ', response);
            //     },
            //     error : function() {

            //     }
            // })
        },
        render: function() {
            /* procesamos notificaciones si hubiera */
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(homeTemplate, {
            });
            this.$el.html(compiledTemplate);
        }
    })
    return HomeView;
})