define([
    'underscore',
    'backbone',
    'JSOG',
    'collections/NivelesJerarquicosCollection',
    'models/NivelJerarquicoModel',
    'mods/modUtils',
    'bootbox',
    'mods/DataTable/DataTable',
    'config/nivelJerarquicoConfig',
    'text!templates/jerarquiaNivel/nivelJerarquicoTemplate.html',
    ], function(
        _,
        Backbone,
        JSOG,
        NivelesJerarquicosCollection,
        NivelJerarquicoModel,
        modUtils,
        bootbox,
        DataTable,
        nivelJerarquicoConfig,
        nivelJerarquicoTemplate
    ) {

    var NivelJerarquicoView = Backbone.View.extend({
        el : '#container',
        initialize : function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            this.nivelesJerarquicos = new NivelesJerarquicosCollection();
            // this.nivelesJerarquicos.on('reset', this.render, this);
            this.nivelesJerarquicos.on('sync', this.render, this);
            // this.nivelesJerarquicos.fetch({ reset : true });
            this.getNivelesJerarquicos();
        },
        getNivelesJerarquicos : function() {
            this.nivelesJerarquicos.fetch();
        },
        template : _.template(nivelJerarquicoTemplate),
        render : function() {
            modUtils.processCustomMessages();
            this.$el.html(this.template({}));
            var config = nivelJerarquicoConfig.getConfig(this);
            this.dataTable = new DataTable();
            this.dataTable.showGrid(this.nivelesJerarquicos.toJSON(), config);
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
        },
        toggle : function(item) {
            this.$('#edit-nivel-jerarquico').toggle();
            this.$('#select-table-jerarquia-nivel_wrapper').toggle();
            if (item) {
                this.curItem = item;
                this.$('#idNivelJerarquico').val(item.idNivelJerarquico);
                this.$('#descripcion-nivel').val(item.descripcionNivelJerarquico);
                this.$('#abreviatura-nivel').val(item.abreviaturaNivelJerarquico);
                this.$('#nivel').val(item.nivel);
            }
        },
        doSave : function(e) {

            e.preventDefault();
            var curItem = JSOG.decode(this.curItem);
            var nivelJerarquicoModel = new NivelJerarquicoModel(curItem);
            nivelJerarquicoModel.set({
                idNivelJerarquico : this.$('#idNivelJerarquico').val(),
                abreviaturaNivelJerarquico : this.$('#abreviatura-nivel').val(),
                descripcionNivelJerarquico : this.$('#descripcion-nivel').val(),
                nivel : this.$('#nivel').val(),
            });

            nivelJerarquicoModel.on("sync", this.getNivelesJerarquicos, this);
            nivelJerarquicoModel.save({}, {
                beforeSend : function(jqXHR, settings) {
                    // console.log('settings.data: ', settings.data);
                },
                success: function(model, response, options) {
                    $.notify('Actualización exitosa', 'success');
                },
                error: function(model, response, options) {
                    console.log('response: ', response);
                    $.notify('ERROR conectándose con el servidor', 'error');
                }
            });
        },
        doCancel : function(e) {
            e.preventDefault();
            this.toggle();
        },
        doEdit : function(curItem) {
            // console.log('curItem: ', curItem);
            this.toggle(curItem);
        },
        doDelete : function() {
            bootbox.alert('<h4 class="text-center"><strong>' +
                            'SE HA DESHABILITADO ESTA CARACTERÍSTICA ' +
                            '</strong></h4>');
        }
    })

    return NivelJerarquicoView;
});