define([
  'underscore',
  'backbone',
  'models/NivelJerarquicoModel'
], function(
        _,
        Backbone,
        NivelJerarquicoModel
    ) {
        var DependenciaOrganizacionalCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/dependenciaOrganizacional",
            model: NivelJerarquicoModel,
            initialize : function(models, options) {
            },
            fetchByNivel : function(options) {
                if (options && options.nivel)
                    options.url = this.url + '/getByNivel/' + options.nivel;
                return Backbone.Collection.prototype.fetch.call(this, options);
            },
            fetchDepsBetweenLevels : function(options) {
                if (options && options.loLvl && options.hiLvl)
                    options.url = this.url + '/findDepsBetweenLevels/' + options.loLvl
                + '/' + options.hiLvl;
                else
                    throw ("debes especificar los parámetros loLvl y hiLvl");
                // console.log('options.url: ', BASE_URL + options.url);
                return Backbone.Collection.prototype.fetch.call(this, options);
            },

        });
        // You don't usually return a collection instantiated
        return DependenciaOrganizacionalCollection;
})