define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ParametroCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/parametros",
            // model: GerenciaModel,
            fetchDependenciaOrganizacionalViewParams : function(options) {
              // console.log('options: ', options);
              if (options && options.nivel) {
                options.url = this.url + '/dependenciaOrganizacionalViewParams/'
                                        + options.nivel
                                        + '/' + options.idDependencia;
              }
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Model.prototype.fetch.call(this, options);
            },
            fetchCargoViewParams : function(options) {
              // console.log('options: ', options);
              if (options && options.idDependencia) {
                options.url = this.url + '/cargoViewParams/'
                                        + options.nivel
                                        + '/' + options.idDependencia;
              }
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Model.prototype.fetch.call(this, options);
            },
            fetchEmpleadoViewParams : function(options) {
              options.url = this.url + '/empleadoViewParams/';
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Model.prototype.fetch.call(this, options);
            },

        });
        // You don't usually return a collection instantiated
        return ParametroCollection;
})