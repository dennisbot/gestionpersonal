define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var EmpleadoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_EMPLEADO,
            url: "rest/empleado",
            // model: EmpleadoModel,
            fetchMinEmpleados : function(options) {
              options.url = this.url + '/toselect/';
              console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Model.prototype.fetch.call(this, options);
            },
        });
        // You don't usually return a collection instantiated
        return EmpleadoCollection;
})