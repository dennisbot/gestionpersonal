define([
  'underscore',
  'backbone',
  'models/NivelJerarquicoModel'
], function(
        _,
        Backbone,
        NivelJerarquicoModel
    ) {
        var NivelesJerarquicosCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/nivelJerarquico",
            model: NivelJerarquicoModel,
            initialize : function(models, options) {
                // console.log('options: ', options);
                if (options && options.idItemEstrategico) {
                    if (options.idItemEstrategico)
                        this.url += '/bymeta/' + options.idItemEstrategico;
                }
                // console.log('this.url: ', this.url);
            },
            fetchLessThanEqualNivel : function(options) {
                if (options && options.nivel)
                    options.url = this.url + '/getLessThanEqualNivel/' + options.nivel;
                return Backbone.Model.prototype.fetch.call(this, options);
            }
        });
        // You don't usually return a collection instantiated
        return NivelesJerarquicosCollection;
})