define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var CargoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/cargo",
            // model: GerenciaModel,
        });
        // You don't usually return a collection instantiated
        return CargoCollection;
})