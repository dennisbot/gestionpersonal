define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var DependenciaOrganizacionalModel = Backbone.Model.extend({
        urlRoot: 'rest/dependenciaOrganizacional',
        idAttribute: "idDependenciaOrganizacional",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
        getNextCodDependencia : function(options) {
            options.url = this.urlRoot + '/nextCodDependencia';
            console.log('options.url: ', BASE_URL + options.url);
            return Backbone.Model.prototype.fetch.call(this, options);
        }
    });
    // Return the model for the module
    return DependenciaOrganizacionalModel;
});