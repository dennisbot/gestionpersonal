define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var NivelJerarquicoModel = Backbone.Model.extend({
        urlRoot: 'rest/nivelJerarquico',
        idAttribute: "idNivelJerarquico",
        defaults: {
            'nivel' : 0,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
        fetchNivel : function(options) {
            if (options && options.nivel)
                options.url = this.urlRoot + '/getNivel/' + options.nivel;
            return Backbone.Model.prototype.fetch.call(this, options);
        }
    });
    // Return the model for the module
    return NivelJerarquicoModel;
});