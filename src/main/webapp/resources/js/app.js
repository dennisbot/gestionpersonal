var el;

var PATH_REST = '';

/* BASE_URL ya está seteado desde main.jsp */

var REST_BASE_PATH_SERVICE = '';
var PATH_SERVICE_GERENCIA = '';
var PATH_SERVICE_SUPERINTENDENCIA = '';

var parametros;

var debug = false;

/* global temp variable to custom debug */
var gg;

define(
        [
            'jquery',
            'jqueryAnimateColor',
            'jquerymigrate',
            'bootstrap',
            'underscore',
            'backbone',
            'bbnotfound',

            'cookiejs',
            'notify',
            'colorbox',
            'moment',
            'fullcalendar',
            'langfullcalendar',
            'bootbox',
            'editinplace',
            // 'subsequenceSearch',

            // 'mods/modSecurity',
            // 'mods/modMenu',
            'AppRouterMappings',
            'collections/ParametrosCollection',

            'text!templates/menuTemplate.html',

            'lte_app',
            'lte_demo',
            'JSOG',
            'mods/modUtils',
            'customExtends'
         ],
		function(
            $,
            jqueryAnimateColor,
            jquerymigrate,
            _bootstrap,
            _,
            Backbone,
            bbnotfound,

            cookiejs,
            notify,
            colorbox,
            moment,
            fullcalendar,
            langfullcalendar,
            bootbox,
            editinplace,
            // subsequenceSearch,

            // modSecurity,
            // modMenu,
            AppRouterMappings,
            ParametrosCollection,

            menuTemplate,

            lte_app,
            lte_demo,
            JSOG,
            modUtils,
            customExtends
        ) {



    var initialize = function() {
        customExtends.initialize();
        lte_app.initialize();
        // console.log('moment: ', moment);
        // console.log('fullcalendar: ', fullcalendar);
        /* establecemos los paths para acceder a los servicios rest  */
        setPaths();
        /* cargamos los parametros e inicializamos las rutas */
        loadParamsAndRoutes();
    }

    var setPaths = function() {
        REST_BASE_PATH_SERVICE = BASE_URL + 'rest/';
        PATH_SERVICE_GERENCIA = REST_BASE_PATH_SERVICE + 'gerencia'
        PATH_SERVICE_SUPERINTENDENCIA = REST_BASE_PATH_SERVICE + 'superintendencia'
    }

    var loadParamsAndRoutes = function() {
        /*console.log('BASE_URL: ', BASE_URL);
        console.log('REST_BASE_PATH_SERVICE: ', REST_BASE_PATH_SERVICE);*/
        (new ParametrosCollection()).fetch({
            success: function(model, response, options) {
                // console.log('se cargo los parametros ...');
                parametros = response;
                // console.log('response: ', response);

                /* por implementar en esta vista */
                // var allowedMenu = modMenu.getPreparedHTMLMenu(parametros.permisos);
                // $('#menu-container').html(allowedMenu);

                // console.log('allowedMenu: ', allowedMenu);
                // var menuText = modMenu.generateHTML(allowedMenu);

                /* cargamos el menu en base al usuario logueado */
                // var compiledTemplate = _.template(menuTemplate, {
                //     modSecurity : modSecurity
                // });
                // $('#menu-container').html(compiledTemplate);

                var usuarioNombre = parametros.usuario.nombres + ' ' +
                    parametros.usuario.paterno;
                var usuarioNombre = modUtils.toTitleCase(usuarioNombre);
                var cargo = parametros.usuario.cargo;
                $('#usuario-sidebar').html(usuarioNombre);
                $('#usuario-small').html(usuarioNombre);
                $('#usuario-perfil').html(usuarioNombre);
                $('#usuario-cargo').html(cargo ? cargo.descripcionCargo : '(cargo por definir)');

                /* inicializamos las rutas para mostrar las interfaces */
                AppRouterMappings.initialize();
            },
            error: function() {
                bootbox.alert('<h4 class="text-center"><strong>hubo un error inicializando</strong></h4>');
                console.log('hubo un error inicializando');
            }
        });
    }

	return {
		initialize: initialize
	};
});