define([
    'AppRouter',
    // 'mods/modSecurity',
    // 'config/confRutas',
    'views/Empleado/EmpleadoView',
    'views/Cargo/CargoView',
    'views/NivelJerarquicoView',
    'views/DependenciaOrganizacional/DependenciaOrganizacionalView',
    'views/admin/MiPerfilView',
    'views/HomeView',
], function(
    AppRouter,
    // modSecurity,
    // confRutas,
    EmpleadoView,
    CargoView,
    NivelJerarquicoView,
    DependenciaOrganizacionalView,
    MiPerfilView,
    HomeView
    ) {
    return {
        appRouter : null,
        // rutas : confRutas.rutas,
        initialize : function() {
            // console.log('parametros: ', parametros);
            // console.log('this.rutas: ', this.rutas);
            // console.log('como objeto:');
            /* mappings :) */
            // var user = parametros.empleado;
            this.appRouter = new AppRouter;
            var self = this;
            /* cuando generemos el menú en base a permisos */
            // this.rutas.forEach(function(ruta) {
            //     if (modSecurity.canView('#' + ruta.urlDescription)
            //         || ruta.urlDescription == '' || ruta.optional)
            //         self.appRouter.route(ruta.urlDescription, ruta.showMethod, self[ruta.showMethod]);
            // })
            this.appRouter.route('empleado', 'showEmpleado', this.showEmpleado);
            this.appRouter.route('cargo(/)(*arguments)', 'showCargo', this.showCargo);
            this.appRouter.route('dependencia-organizacional(/)(*arguments)', 'showDependenciaOrganizacional', this.showDependenciaOrganizacional);
            this.appRouter.route('nivel-jerarquico', 'showNivelJerarquico', this.showNivelJerarquico);
            this.appRouter.route('mi-perfil', 'showMiPerfil', this.showMiPerfil);
            this.appRouter.route('', 'homeAction', this.homeAction);
            Backbone.history.start();
        },
        showEmpleado : function() {
            ViewManager.handle(new EmpleadoView({
            }));
        },
        showCargo : function(arguments) {
            var args = arguments ? arguments.split('/') : [], nivel = 1, idDependencia;
            if (args.length) {
                nivel = parseInt(args.shift());
                if (args.length)
                    idDependencia = parseInt(args.shift());
            }
            if (!idDependencia) {
                /* ponemos en una cookie para que en la sgte redir se muestre la razón de la redir */
                $.cookie('errorMessage', 'ERROR, no tienes un cargo asignado, solicita a tu jefe inmediato ' +
                'superior que te asigne un cargo en el sistema');
                // Backbone.history.navigate('/mi-perfil', true);
                window.location.replace(BASE_URL);
                return false;
            }
            ViewManager.handle(new CargoView({
                nivel : nivel,
                idDependencia : idDependencia
            }));
        },
        showDependenciaOrganizacional : function(arguments) {
            var args = arguments ? arguments.split('/') : [], nivel = 1, idDependencia;
            if (args.length) {
                nivel = parseInt(args.shift());
                if (args.length)
                    idDependencia = parseInt(args.shift());
            }
            var dependenciaOrganizacionalView = new DependenciaOrganizacionalView({
                nivel : nivel,
                idDependencia : idDependencia
            });
            ViewManager.handle(dependenciaOrganizacionalView);
            // dependenciaOrganizacionalView.render();
        },
        showNivelJerarquico : function() {
            var nivelJerarquicoView = new NivelJerarquicoView();
            ViewManager.handle(nivelJerarquicoView);
            // nivelJerarquicoView.render();
        },
        showMiPerfil : function() {
            var miPerfilView = new MiPerfilView();
            ViewManager.handle(miPerfilView);
            miPerfilView.render();
            // this.selectItem('#mi-perfil');
        },
        homeAction : function(actions) {
            // console.log('actions: ', actions);
            var homeView = new HomeView();
            ViewManager.handle(homeView);
            // console.log('homeView: ', homeView);
            // We have no matching route, lets display the home page
            // this.selectItem('#' + actions);
            // this.appRouter.navigate('/', true);
        }
    };
});