define([
    'underscore',
    'text!templates/loadingTemplate.html'
], function(
    _,
    loadingTemplate
) {
    return {
        initialize : function() {
            window.ViewManager = {
                // A property to store the current view being displayed.
                currentView: null,
                // Display a Backbone View. Closes the previously displayed view gracefully.
                handle: function(view) {
                    // Close the previous view
                    if (this.currentView != null) {
                        // Invoke the close method on the view.
                        // All views have this method, defined via the 'Backbone.View.prototype.close' method.
                        this.currentView.close();
                        this.currentView.$el.html(_.template(loadingTemplate, {}));
                        // console.log('this.currentView.$el: ', this.currentView.$el);
                    }
                    // Display the current view
                    this.currentView = view;
                    // return this.currentView.render();
                }
            }
            /* aún no usado */
            Backbone.View.prototype.myrender = function() {
                if (this.beforeRender) this.beforeRender()
                Backbone.View.prototype.render.apply(this, arguments);
                if (this.afterRender) this.afterRender();

            }
            /* esto si se usa bastante */
            Backbone.View.prototype.close = function() {
                if (this.onClose) this.onClose();
                /* this was before bb 1.1.0 */
                /*
                this.remove();
                this.unbind();
                */
                /* from bb 1.1.0 and above */
                // When the view is displayed again, a new instance of it is used.
                // New event listeners are ths added. Hence, stop delegating events for the current view.
                // This ensures the listeners in this view are no longer triggered.
                // We don't invoke 'this.remove()' since the View must not remove elements from DOM.
                this.undelegateEvents();
            }

        }
    }
});