define([
        'underscore',
        'mods/modUtils',
        'text!templates/dependenciaOrganizacional/columnaOpsDependenciaOrganizacionalTemplate.html',
    ],
    function (
        _,
        modUtils,
        columnaOpsDependenciaOrganizacionalTemplate
    ) {

    var dependenciaOrganizacionalConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de jerarquiaOrgConfig.js');
            var config = {
                rowId : 'idDependenciaOrganizacional',
                order : [[2, 'asc']],
                container : '#select-table-dependencia-organizacional',
                $container : context.$('#select-table-dependencia-organizacional'),
                doEdit : _.bind(context.doEdit, context),
                doDelete : _.bind(context.doDelete, context),
                doMove : _.bind(context.doMove, context),
                columns : [
                    {
                        data : 'idDependenciaOrganizacional',
                        // width: '15%',
                        orderable : false,
                        sClass : 'alinear-vertical text-center',
                    },
                    {
                        data : 'descripcionDependencia',
                        sClass : 'alinear-vertical',
                    },
                    {
                        data : 'abreviaturaDependencia',
                        sClass : 'alinear-vertical',
                    },
                    {
                        data : function(row, type, set) {
                            if (row.dependenciaOrganizacional)
                                return row.dependenciaOrganizacional.descripcionDependencia;
                            return '(raiz)';

                        },
                        sClass : 'alinear-vertical text-center',
                        // width : '10%',
                    },
                    {
                        data : 'nivelJerarquico.nivel',
                        // width: '25%',
                        orderable : false,
                        sClass : 'alinear-vertical text-center',
                    }
                ],
                columnDefs : [
                    {
                        'targets' : 0,
                        'data' : null,
                        'mRender' : function(data, type, row) {
                            return _.template(columnaOpsDependenciaOrganizacionalTemplate, {
                                dependencia : row,
                                context : context
                            })
                        }
                    }
                ]
            }
            return config;
        }
    }

    return dependenciaOrganizacionalConfig;
});