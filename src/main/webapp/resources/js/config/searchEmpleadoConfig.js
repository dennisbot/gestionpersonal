define([
        'underscore',
        'text!templates/util/searchSelectEmpleadoTemplate.html',
    ],
    function (
        _,
        searchSelectEmpleadoTemplate
    ) {

    var searchEmpleadoConfig = {
        getConfig: function(context) {
            // console.log('response: ', response);
            // console.log('entra a getConfig de searchEmpleadoConfig');
            return {
                rowId: 'idEmpleado',
                hasDetails: true,
                paging: true,
                bFilter: false,
                order: [[0, 'asc']],
                container: '#table-result-search-empleado',
                $container: $('#table-result-search-empleado'),
                doSelect : _.bind(context.doSelectSearch, context),
                doEdit : _.bind(context.doEdit, context),
                columns: [
                    {
                        data : 'descripcionItemResult',
                        // width : '40%',
                    },
                    {
                        data : function(row, type, set) {
                            if (row.cargo)
                                return row.cargo.descripcionCargo;
                            return '(por definir)';
                        },
                        // width : '40%',
                    },
                    {
                        data : 'idEmpleado',
                        width : '30%',
                        sClass : 'text-center',
                        orderable: false,
                    }
                ],
                columnDefs: [
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            if (row.cargo)
                                return _.template(searchSelectEmpleadoTemplate, { empleado : row , showEdit : context.showEdit });
                            var html = context.showEdit ? '<a href="#" class="obj-edit pull-left" title="editar"><i class="fa fa-pencil"></i> Editar</a>' : '';
                            var pr = context.showEdit ? ' clas="pull-right"' : '';
                            return html + '<span' + pr + '>(dependencia por definir)</span>';
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                    },
                ],
                lengthMenu : [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            }
        }
    }

    return searchEmpleadoConfig;
});