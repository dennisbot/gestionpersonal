define([
        'underscore',
        'mods/modProgressBar',
        'text!templates/gestion/niveles/iniciativas/control/columnaOpsControlIniciativaTemplate.html',
    ],
    function (
        _,
        modProgressBar,
        columnaOpsControlIniciativaTemplate,
        avanceIniciativaTemplate
    ) {

    var iniciativaConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de iniciativaConfig');
            return {
                rowId : 'idItemEstrategico',
                hasDetails : true,
                formatDataTableDetails: _.bind(context.formatDataTableDetails, context),
                onChildRowRendered : _.bind(context.onChildRowRendered, context),
                order: [[3, 'desc']],
                container: '.data-table-container',
                $container: context.$('.data-table-container'),
                columns: [
                    {
                        sClass: 'details-control',
                        orderable : false,
                        data : null,
                        defaultContent : '',
                    },
                    {
                        data : 'descripcionItem',
                        // width : '40%',
                    },
                    {
                        data : 'dependenciaOrganizacional.descripcionDependencia',
                        // width : '40%',
                    },
                    {
                        data : '',
                        sClass: 'alinear-vertical text-center',
                        width : '20%',
                    },
                    /*{
                        data : 'idItemEstrategico',
                        width : '10%',
                        orderable: false,
                    },*/
                ],
                columnDefs: [
                    {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            // console.log('row: ', row;
                            // return modProgressBar.draw(row, 4);
                            modProgressBar.curPeriodo = null;
                            return modProgressBar.getInstance(4, row).draw();
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                     /*{
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsControlIniciativaTemplate, {
                                idItemEstrategico : data,
                                nivel : row.descripcionNivel.idNivel,
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },*/

                 ],
            }
        }
    }

    return iniciativaConfig;
});