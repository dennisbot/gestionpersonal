define({
        rutas :  [
                {
                    urlDescription : 'manage-levels',
                    showMethod : 'showManageLevels'
                },
                {
                    urlDescription : 'manage-unidad-medida',
                    showMethod : 'showManageUnidadMedida'
                },
                {
                    urlDescription : 'manage-tipo-metrica',
                    showMethod : 'showManageTipoMetrica'
                },
                {
                    urlDescription : 'manage-metrica',
                    showMethod : 'showManageMetrica'
                },
                {
                    urlDescription : 'manage-categoria',
                    showMethod : 'showManageCategoria'
                },
                {
                    urlDescription : 'objetivos-estrategicos',
                    showMethod : 'showObjetivosEstrategicos'
                },
                {
                    urlDescription : 'objetivos-clave',
                    showMethod : 'showObjetivosClave'
                },
                {
                    urlDescription : 'objetivos-especificos',
                    showMethod : 'showObjetivosEspecificos'
                },
                {
                    urlDescription : 'iniciativas',
                    showMethod : 'showIniciativas'
                },
                {
                    urlDescription : 'control-iniciativas',
                    showMethod : 'showControlIniciativas'
                },
                {
                    urlDescription : 'top10-superintendencias',
                    showMethod : 'showTop10Superintendencias'
                },
                {
                    urlDescription : 'mi-perfil',
                    optional : true,
                    showMethod : 'showMiPerfil'
                },
                {
                    urlDescription : 'manage-permisos',
                    showMethod : 'showManagePermisos'
                },
                {
                    urlDescription : 'asignacion-roles',
                    showMethod : 'showAsignacionRoles'
                },
                {
                    urlDescription : '',
                    showMethod : 'homeAction'
                }
            ]
});