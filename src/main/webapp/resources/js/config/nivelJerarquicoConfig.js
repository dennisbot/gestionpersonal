define([
        'underscore',
        'mods/modUtils',
        'text!templates/jerarquiaNivel/columnaOpsNivelJerarquicoTemplate.html',
    ],
    function (
        _,
        modUtils,
        columnaOpsNivelJerarquicoTemplate
    ) {

    var dependenciaOrgConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de jerarquiaOrgConfig.js');
            var config = {
                rowId : 'idNivelJerarquico',
                order : [[2, 'asc']],
                container : '.data-table-container',
                $container : context.$('.data-table-container'),
                doEdit : _.bind(context.doEdit, context),
                doDelete : _.bind(context.doDelete, context),
                columns : [
                    {
                        data : 'descripcionNivelJerarquico',
                        sClass : 'alinear-vertical',
                    },
                    {
                        data : 'abreviaturaNivelJerarquico',
                        sClass : 'alinear-vertical',
                    },
                    {
                        data : 'nivel',
                        sClass : 'alinear-vertical text-center',
                        width : '10%',
                    },
                    {
                        data : 'idNivelJerarquico',
                        width: '17%',
                        orderable : false,
                        sClass : 'alinear-vertical text-center',
                    },
                ],
                columnDefs : [
                    {
                        'targets' : -1,
                        'data' : null,
                        'mRender' : function(data, type, row) {
                            return _.template(columnaOpsNivelJerarquicoTemplate, {
                                idNivelJerarquico : data
                            })
                        }
                    }
                ]
            }
            return config;
        }
    }

    return dependenciaOrgConfig;
});