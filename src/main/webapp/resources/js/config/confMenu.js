define(function() {
    return {
        menu : [
            {
                nodeType : 'ul',
                className : 'nav navbar-nav',
                idName :'menuContainer',
                attributes : [],
                items : [
                    {
                        nodeType : 'li-container-anchor-ul',
                        idLiContainer : 'menu-gestion',
                        anchorClassName : 'dropdown-toggle',
                        liContainerClassName : '',
                        href : '#gestion',
                        anchorAttributes : [
                            'tabindex="-1"',
                            'data-toggle="dropdown"',
                            'role="button"',
                            'aria-expanded="false"',
                        ],
                        textElement : ' GESTIÓN <span class="caret"></span>',
                        ulClass : 'dropdown-menu',
                        ulAttributes : [
                            'role="menu"',
                        ],
                        items : [
                            {
                                nodeType : 'group',
                                className : 'divider',
                                headerClassName : 'dropdown-header',
                                textElement : 'GESTIÓN DE PARÁMETROS',
                                items : [
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#manage-levels',
                                        textElement : '<i class="fa fa-bell-o"></i> Niveles',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#manage-unidad-medida',
                                        textElement : '<i class="fa fa-bell-o"></i> Unidades de medida',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#manage-tipo-metrica',
                                        textElement : '<i class="fa fa-bell-o"></i> Tipos de KPIs',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#manage-metrica',
                                        textElement : '<i class="fa fa-bell-o"></i> KPIs',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#manage-categoria',
                                        textElement : '<i class="fa fa-bell-o"></i> Categorías',
                                    },
                                ]
                            },
                            {
                                nodeType : 'group',
                                className : 'divider',
                                headerClassName : 'dropdown-header',
                                textElement : 'JERARQUÍA / NIVELES ESTRATÉGICOS',
                                items : [
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#objetivos-estrategicos',
                                        textElement : '<i class="fa fa-tasks"></i> 1.- Mostrar Obj. Estratégicos',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#objetivos-clave',
                                        textElement : '<i class="fa fa-tasks"></i> 2.- Mostrar Obj. Clave',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#objetivos-especificos',
                                        textElement : '<i class="fa fa-tasks"></i> 3.- Mostrar Obj. Específicos',
                                    },
                                    {
                                        nodeType : 'liAnchor',
                                        href : '#iniciativas',
                                        textElement : '<i class="fa fa-tasks"></i> 4.- Iniciativas',
                                    },
                                ]
                            },
                        ]
                    },
                    {
                        nodeType : 'li-container-anchor-ul',
                        idLiContainer : '',
                        anchorClassName : 'dropdown-toggle',
                        liContainerClassName : '',
                        href : '#avance',
                        anchorAttributes : [
                            'tabindex="-1"',
                            'data-toggle="dropdown"',
                            'role="button"',
                            'aria-expanded="false"',
                        ],
                        textElement : ' AVANCES <span class="caret"></span>',
                        ulClass : 'dropdown-menu',
                        ulAttributes : [
                            'role="menu"',
                        ],
                        items : [
                            {
                                nodeType : 'liAnchor',
                                href : '#control-iniciativas',
                                textElement : '<i class="fa fa-user"></i> CONTROL DE INICIATIVAS',
                            },
                        ]
                    },
                    {
                        nodeType : 'li-container-anchor-ul',
                        idLiContainer : '',
                        anchorClassName : 'dropdown-toggle',
                        liContainerClassName : '',
                        href : '#avance',
                        anchorAttributes : [
                            'tabindex="-1"',
                            'data-toggle="dropdown"',
                            'role="button"',
                            'aria-expanded="false"',
                        ],
                        textElement : ' REPORTES <span class="caret"></span>',
                        ulClass : 'dropdown-menu',
                        ulAttributes : [
                            'role="menu"',
                        ],
                        items : [
                            {
                                nodeType : 'liAnchor',
                                href : '#top10-superintendencias',
                                textElement : '<i class="fa fa-user"></i> TOP 10 SUPERINTENDENCIAS',
                            },
                            {
                                nodeType : 'liAnchor',
                                href : '#top10-gerencias',
                                textElement : '<i class="fa fa-calendar"></i> TOP 10 GERENCIAS',
                            },
                            {
                                nodeType : 'liAnchor',
                                href : '#top10-vicepresidencias',
                                textElement : '<i class="fa fa-calendar"></i> TOP 10 VICEPRESIDENCIAS',
                            },
                        ]
                    },
                ]
            },
            {
                nodeType : 'ul',
                className : 'nav navbar-nav navbar-right',
                idName :'',
                attributes : [],
                items : [
                    {
                        nodeType : 'li-container-anchor-ul',
                        idLiContainer : '',
                        anchorClassName : 'dropdown-toggle',
                        liContainerClassName : 'page',
                        href : '#',
                        anchorAttributes : [
                            'tabindex="-1"',
                            'data-toggle="dropdown"',
                            'role="button"',
                            'aria-expanded="false"',
                        ],
                        textElement : '<i class="fa fa-user fa-lg"></i> <span id="username">username</span> <span class="caret"></span>',
                        ulClass : 'dropdown-menu',
                        ulAttributes : [
                            'role="menu"',
                        ],
                        items : [
                            {
                                nodeType : 'liAnchor',
                                optional : true,
                                href : '#mi-perfil',
                                textElement : '<i class="fa fa-lock"></i> MI PERFIL',
                            },
                            {
                                nodeType : 'liAnchor',
                                href : '#manage-permisos',
                                textElement : '<i class="fa fa-check-square-o"></i> GESTIÓN DE PERMISOS',
                            },
                            {
                                nodeType : 'liAnchor',
                                href : '#asignacion-roles',
                                textElement : '<i class="fa fa-group"></i> ASIGNACIÓN DE ROLES',
                            },
                            {
                                nodeType : 'liAnchor',
                                optional : true,
                                href : 'logout',
                                textElement : '<i class="fa fa-power-off"></i> CERRAR SESIÓN',
                            },
                        ]
                    },
                ]
            },
        ]
    }
});