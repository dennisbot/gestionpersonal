define([
        'underscore',
        'text!templates/util/searchSelectDependenciaTemplate.html',
    ],
    function (
        _,
        searchSelectDependenciaTemplate
    ) {

    var searchDependenciaConfig = {
        getConfig: function(context) {
            // console.log('response: ', response);
            // console.log('entra a getConfig de searchDependenciaConfig');
            return {
                rowId: 'idDependenciaOrganizacional',
                hasDetails: true,
                paging: true,
                bFilter: false,
                order: [[0, 'asc']],
                container: '#table-result-search-dependencia',
                $container: $('#table-result-search-dependencia'),
                doSelect : _.bind(context.doSelectSearch, context),
                columns: [
                    {
                        data : 'descripcionItemResult',
                        // width : '40%',
                    },
                    {
                        data : function(row, type, set) {
                            if (row.dependenciaOrganizacional)
                                return row.dependenciaOrganizacional.descripcionDependencia;
                            return '(raiz)';
                        },
                        // width : '40%',
                    },
                    {
                        data : 'idDependenciaOrganizacional',
                        width : '20%',
                        sClass : 'text-center',
                        orderable: false,
                    },
                ],
                columnDefs: [
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(searchSelectDependenciaTemplate, { dependencia : row });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                    },
                ],
                lengthMenu : [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            }
        }
    }

    return searchDependenciaConfig;
});