define([
        'underscore',
        'text!templates/gestion/niveles/oclaves/columnaOpsOClaveTemplate.html',
        'mods/modSecurity',
    ],
    function (
        _,
        columnaOpsOClaveTemplate,
        modSecurity
    ) {

    var oclaveConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de oclaveConfig');
            var config = {
                container: '.data-table-container',
                $container: context.$('.data-table-container'),
                order: [[1, 'asc']],
                doGo: _.bind(context.doGo, context),
                doEdit: _.bind(context.doEdit, context),
                doDelete: _.bind(context.doDelete, context),
                columns: [
                    {
                        data : 'descripcionNivel.idNivel',
                        sClass: 'alinear-vertical text-center',
                        width: '5%',
                        orderable: false,
                    },
                    {
                        data : 'descripcionItem',
                        // width : '40%',
                    },
                    {
                        data : 'jerarquiaOrganizacional.descripcionDependencia',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }

            // console.log('context.curUrlDescripcion: ', context.curUrlDescripcion);
            // var canedit = modSecurity.canEdit(context.curUrlDescripcion);
            // var candelete = modSecurity.canDelete(context.curUrlDescripcion);
            // var canview = modSecurity.canView(context.curUrlDescripcion);

            // console.log('canedit: ', canedit);
            // console.log('candelete: ', candelete);
            // console.log('canview: ', canview);

            if (modSecurity.canEdit(context.curUrlDescripcion) ||
                modSecurity.canDelete(context.curUrlDescripcion) ||
                modSecurity.canView(context.curUrlDescripcion) ) {
                config.columns.push({
                    data : 'idItemEstrategico',
                    width : '25%',
                    orderable: false,
                    sClass : 'alinear-vertical text-center'
                });
                config.columnDefs.push({
                    "targets": -1,
                    "data": null,
                    "mRender": function (data, type, row) {
                        return _.template(columnaOpsOClaveTemplate, {
                            idItemEstrategico : data,
                            nivel : row.descripcionNivel.idNivel,
                            modSecurity : modSecurity,
                            curUrlDescripcion : context.curUrlDescripcion
                        });
                    }
                    // "defaultContent": '<button class="btn btn-default">Click!</button>'
                 });
            }

            return config;
        }
    }

    return oclaveConfig;
});