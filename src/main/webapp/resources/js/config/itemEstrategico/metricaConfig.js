define([
        'underscore',
        'text!templates/metrica/columnaSelectMetricaTemplate.html',
    ],
    function (
        _,
        columnaSelectMetricaTemplate
    ) {

    var metricaConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de metricaConfig');
            return {
                    rowId: 'idMetrica',
                    paging: true,
                    bFilter: true,
                    hasDetails: true,
                    formatDataTableDetails: _.bind(context.formatDataTableDetails, context),
                    order: [[1, 'asc']],
                    container: '#select-table-metrica',
                    $container: $('#select-table-metrica'),
                    doSelect : _.bind(context.doSelect, context),
                    columns: [
                        {
                            sClass: 'details-control',
                            orderable : false,
                            data : null,
                            defaultContent : '',
                        },
                        {
                            data : 'descripcionMetrica',
                            sClass: 'alinear-vertical',
                            // width : '20%',
                        },
                        {
                            data : 'unidadMedida.mostrarUnidadAbrev',
                            sClass: 'alinear-vertical',
                            // width : '40%',
                        },
                        {
                            data : 'formula',
                            sClass: 'alinear-vertical',
                            // width : '40%',
                        },
                        {
                            data : 'tipoMetrica.descripcionTipoMetrica',
                            sClass: 'alinear-vertical',
                            // width : '40%',
                        },
                        {
                            data : 'idMetrica',
                            sClass: 'alinear-vertical text-center',
                            width : '15%',
                            orderable: false,
                        },
                    ],
                    columnDefs: [
                         {
                            "targets": -1,
                            "data": null,
                            "mRender": function (data, type, row) {
                                return "";
                                // return _.template(columnaSelectMetricaTemplate, { idMetrica : data });
                            }
                            // "defaultContent": '<button class="btn btn-default">Click!</button>'
                         },
                    ],
            }
        }
    }

    return metricaConfig;
});