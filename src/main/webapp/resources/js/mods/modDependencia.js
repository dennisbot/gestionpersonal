define(
    [
        'mods/dependencia/childs/_01Corporacion',
        'mods/dependencia/childs/_02Presidencia',
        'mods/dependencia/childs/_03VicePresidencia',
        'mods/dependencia/childs/_04Gerencia',
        'mods/dependencia/childs/_05Superintendencia',
        'mods/dependencia/childs/_06Area',
    ],
    function(
        Corporacion,
        Presidencia,
        VicePresidencia,
        Gerencia,
        Superintendencia,
        Area
    ) {
        return {
            getInstance : function(nivel) {
                switch(nivel) {
                    case 1:
                        return new Corporacion();
                    case 2:
                        return new Presidencia();
                    case 3:
                        return new VicePresidencia();
                    case 4:
                        return new Gerencia();
                    case 5:
                        return new Superintendencia();
                    case 6:
                        return new Area();
                }
                return null;
            }
        }
})