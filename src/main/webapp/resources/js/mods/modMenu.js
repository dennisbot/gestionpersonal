define(['config/confMenu'], function(confMenu) {
   return {
        menu : confMenu.menu,
        filter : function(permisos) {
            // console.log('permisos: ', permisos);
            var self = this;
            this.mark(this.menu, permisos);
            // console.log('this.menu: ', this.menu);
            // console.log('se queda');
            // return ;
            this.menu.forEach(function(nodeGroup) {
                self.propagateCanView(nodeGroup);
            })
        },
        mark : function(items, permisos) {
            var self  = this;
            items.forEach(function(item) {
                item.canView = item.optional || item.href && permisos[item.href] && permisos[item.href].canView;
                (item.items) ? self.mark(item.items, permisos) : '';
            })
        },
        propagateCanView : function(node) {
            var canView = false;
            if (node.items && node.items instanceof Array) {
                for (var i = 0; i < node.items.length; i++) {
                    var item = node.items[i];
                    var curCanView = item.canView ? true : this.propagateCanView(item);
                    item.canView = curCanView;
                    canView = canView || curCanView;
                }
            }
            node.canView = canView;
            return canView;
        },

        getPreparedHTMLMenu : function(permisos) {
            this.filter(permisos);
            var self = this;
            // console.log('this.menu: ', this.menu);
            var menu = '';
            this.menu.forEach(function(nodeGroup) {
                menu += self.generateMenu(nodeGroup);
            })
            return menu;
        },
        generateMenu : function(node) {
            switch(node.nodeType) {
                case 'ul':
                    return node.canView ? '<ul class="' + node.className + '" ' +
                    (node.idName ? 'id="' + node.idName + '" ' : '') +
                    (node.attributes.length ? this.getAttributesAsString(node.attributes) : '') +
                    '>'+
                    (node.items ? this.generateItemsMenu(node.items) : '') +
                    '</ul>' : '';
                    break;
                case 'li-container-anchor-ul':
                    return node.canView ? '<li ' +
                    (node.idLiContainer ? 'id="' + node.idLiContainer + '" ' : '') +
                    '>'+
                    '<a href="' + node.href + '" ' +
                    'class="' + node.anchorClassName + '" ' +
                    (node.anchorAttributes.length ? this.getAttributesAsString(node.anchorAttributes) : '') +
                    '>' + node.textElement +
                    '</a>' +
                    '<ul class="' + node.ulClass + '" ' +
                    (node.ulAttributes.length ? this.getAttributesAsString(node.ulAttributes) : '') +
                    '>' + (node.items ? this.generateItemsMenu(node.items) : '') + '</ul>' +
                    '</li>' : '';
                    break;
                case 'group':
                    return node.canView ? '<li class="' + node.className + '"></li>' +
                            '<li class="' + node.headerClassName + '">' + node.textElement + '</li>' +
                            (node.items ? this.generateItemsMenu(node.items) : '') : '';
                    break;
                case 'liAnchor':
                    return node.canView ? '<li><a href="' + node.href + '">' + node.textElement + '</a></li>' : '';
                    break;
            }
            return '';
        },
        generateItemsMenu : function(items) {
            var menuAsString = '';
            var self = this;
            items.forEach(function(item) {
               menuAsString += self.generateMenu(item)
            })
            return menuAsString;
        },
        getAttributesAsString : function(attributes) {
           var attr = '' ;
           attributes.forEach(function(attribute) {
                attr += attribute + ' ';
           })
           if (attr.length) attr = attr.substr(0, attr.length - 1);
           return attr;
        }

   }
});