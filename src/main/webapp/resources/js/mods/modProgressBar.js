define(
    [
        'mods/progressBar/childs/ProgressBarIniciativa',
        'mods/progressBar/childs/ProgressBarMeta',
    ],
    function(
        ProgressBarIniciativa,
        ProgressBarMeta
    ) {
        return {
            getInstance : function(lvl, itemEstrategico) {
                switch(lvl) {
                    case 4:
                        return new ProgressBarIniciativa(itemEstrategico, this.curPeriodo);
                    case 5:
                        itemEstrategico.parentItem = this.iniciativa;
                        return new ProgressBarMeta(itemEstrategico);
                }
                return null;
            }
        }
})