define(function() {

    function callAJAX(options) {
      // console.log('options: ', options);
      $.ajax({
        url : options.url,
        type : options.type ?  options.type : 'GET',
        data : options.jsonVars ? options.jsonVars : {},
        async: options.async ? options.async : true,
        beforeSend : function() {
          // console.log('beforeSend llamado');
          if (options.selectorLoading)
            $(options.selectorLoading).show();
          else
            $('.miloading').show();
        },
        success : function(data) {
          if (options.callback)
              options.callback(data,
                      options.context,
                      options.selectorContainer,
                      options.triggerButton
              );
          else
            console.log('no existe una función callback definida');
          if (options.selectorLoading)
            $(options.selectorLoading).hide();
          else
            $('.miloading').hide();

          if (options.triggerButton) {
            options.triggerButton.removeClass("disabled");
          }
        },
        error: function(data) {
          if (options.onerror)
              options.onerror(
                      data,
                      options.selectorContainer || options.triggerButton
              );

          if (options.selectorLoading)
            $(options.selectorLoading).html("<strong>ERROR</strong>: " + data.responseText);
          else
            $('.miloading').html("<strong>ERROR</strong>: " + data.responseText);

          if (options.triggerButton) {
            options.triggerButton.removeClass("disabled");
          }
        }
      });
    }
    function callAJAXPostJSON(options) {
      // url, jsonVars, callback, context
      $.ajax({
        url : options.url,
        type : options.type ? options.type : 'POST',
        processData: false,
        contentType: "application/json; charset=utf-8",
        data : JSON.stringify(options.jsonVars),
        dataType : "json",
        beforeSend : function(jqXHR, settings) {
          console.log('settings.data: ', settings.data);
          // console.log('beforeSend llamado');
          if (options.selectorLoading)
            $(options.selectorLoading).show();
          else
            $('.miloading').show();
        },
        success : function(data) {
          // console.log('se llamo a la función success de ajax post json');
          if (options.selectorLoading)
            $(options.selectorLoading).hide();
          else
            $('.miloading').hide();
          if (options.callback)
            options.callback(data,
                              options.context,
                              options.selectorContainer,
                              options.triggerButton
            );

          if (options.triggerButton) {
            options.triggerButton.removeClass("disabled");
          }
        },
        error: function(data) {
          if (options.selectorLoading)
            $(options.selectorLoading).html("<strong>ERROR</strong>: " + data.responseText);
          else
            $('.miloading').html("<strong>ERROR</strong>: " + data.responseText);
          if (options.onerror)
              options.onerror(data, options.selectorContainer);

          if (options.triggerButton) {
            options.triggerButton.removeClass("disabled");
          }

          // console.log('hubo un error en la solicitud :(');
          // console.log('data: ', data);
          // gg = data;
        }
      });
    }
    return {
        callAJAX : callAJAX,
        callAJAXPostJSON : callAJAXPostJSON,
    };
});