define(function() {
    return {
        canView : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canView : false;
        },
        canEdit : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canEdit : false;
        },
        canDelete : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canDelete : false;
        },
        canCreate : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canCreate : false;
        }
    }
});