define([
        'text!templates/dependenciaOrganizacional/addNewButtonTemplate.html'
    ],
    function(
        addNewButtonTemplate
    ) {

    function Dependencia() {
        this.backcolor = '';
        this.forecolor = '';
        this.tolerancia = 0;
        this.isYTD = false;
    }

    Dependencia.prototype = {
        getNextLevelDescription : function() {
            return "view/add SubDependencias"
        },
        getDescripcionHeaderNivelDependencia : function(nivelJerarquico) {
            return 'Subdependencias - Nivel ' + nivelJerarquico.nivel;
        },

        getNewDependenciaButton : function(nivelJerarquico) {
            return _.template(addNewButtonTemplate, {
                descripcionDependencia : 'NUEVA SUB DEPENDENCIA'
                // descripcionDependencia : 'NUEVA ' + nivelJerarquico.descripcionNivelJerarquico
            });
        }
    };
    return Dependencia;
});