define(
    [
        'underscore',
        '../Dependencia',
    ], function(
        _,
        Dependencia
    ) {

    function VicePresidencia() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    VicePresidencia.prototype = Object.create(Dependencia.prototype);

    // VicePresidencia.prototype.getDescripcion = function() {
    //     return "la vicepresidencia";
    // }

    return VicePresidencia;
});