define(
    [
        'underscore',
        '../Dependencia'
    ], function(
        _,
        Dependencia
    ) {

    function Area() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    Area.prototype = Object.create(Dependencia.prototype);

    // Area.prototype.getDescripcion = function() {
    //     return "el área";
    // }

    return Area;
});