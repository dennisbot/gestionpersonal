define(
    [
        'underscore',
        '../Dependencia'
    ], function(
        _,
        Dependencia
    ) {

    function Superintendencia() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    Superintendencia.prototype = Object.create(Dependencia.prototype);

    // Superintendencia.prototype.getDescripcion = function() {
    //     return "la superintendencia";
    // }

    return Superintendencia;
});