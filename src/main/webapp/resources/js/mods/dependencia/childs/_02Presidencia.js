define(
    [
        'underscore',
        '../Dependencia',
    ], function(
        _,
        Dependencia
    ) {

    function Presidencia() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    Presidencia.prototype = Object.create(Dependencia.prototype);

    // Presidencia.prototype.getDescripcion = function() {
    //     return "la presidencia";
    // }

    return Presidencia;
});