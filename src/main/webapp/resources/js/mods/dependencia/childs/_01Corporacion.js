define(
    [
        'underscore',
        '../Dependencia',
    ], function(
        _,
        Dependencia
    ) {

    function Corporacion() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    Corporacion.prototype = Object.create(Dependencia.prototype);

    // Corporacion.prototype.getDescripcion = function() {
    //     return "la corporación";
    // }

    return Corporacion;
});