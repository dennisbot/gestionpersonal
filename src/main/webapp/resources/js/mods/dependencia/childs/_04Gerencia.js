define(
    [
        'underscore',
        '../Dependencia',
    ], function(
        _,
        Dependencia
    ) {

    function Gerencia() {
        Dependencia.call(this);
        /* properties -> */
        /* this.param = param */
    }

    Gerencia.prototype = Object.create(Dependencia.prototype);

    // Gerencia.prototype.getDescripcion = function() {
    //     return "la gerencia";
    // }

    return Gerencia;
});