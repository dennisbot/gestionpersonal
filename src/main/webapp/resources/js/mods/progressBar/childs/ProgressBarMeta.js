define(
    [
        'underscore',
        '../ProgressBar',
        'text!templates/gestion/niveles/iniciativas/control/avanceProgressBarTemplate.html'
    ], function(
        _,
        ProgressBar,
        avanceProgressBarTemplate
    ) {

    function ProgressBarMeta(itemEstrategico) {
        ProgressBar.call(this);
        this.itemEstrategico = itemEstrategico;
        this.avance = this.getAvanceMeta();
        this.tolerancia = this.itemEstrategico.parentItem.tolerancia;
    }

    ProgressBarMeta.prototype = Object.create(ProgressBar.prototype);

    ProgressBarMeta.prototype.getAvance = function() {
        return this.avance;
    }

    ProgressBarMeta.prototype.getAvanceMeta = function() {
        return this.itemEstrategico.valorPlaneado == 0
            ? 0
            : Math.round((this.itemEstrategico.valorReal / this.itemEstrategico.valorPlaneado) * 1000) / 10;
    }

    // ProgressBarMeta.prototype.getBackColor = function() {
    //     this.avance += this.itemEstrategico.parentItem.tolerancia;
    //     return ProgressBar.prototype.getBackColor();
    // }

    ProgressBarMeta.prototype.draw = function() {
        var backcolor = 'background-color:' + this.getBackColor();
        var forecolor = 'color:' + this.getForeColor();
        var style = backcolor + ';' + forecolor + (this.isBold() ? ';font-weight:bold' : '');
        return _.template(avanceProgressBarTemplate, {
            avance : this.avance,
            style: style
        });
    }

    return ProgressBarMeta;
});