define(function() {
    var showErrorMessages = function(Messages) {
        var msg = "Se encontraron los siguientes errores:";
        for (var i = 0; i < Messages.length; i++) {
            msg += "\n" + Messages[i];
        }
        return msg;
    }

    var requerido = function(fieldValue) {
        return fieldValue && fieldValue.toString().trim() != "";
    }

    var isEmail = function(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    var fechaValida = function(fieldValue) {
        // First check for the pattern
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(fieldValue))
            return false;

        // Parse the date parts to integers
        var parts = fieldValue.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if(year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }

    var valid_percentage = function(fieldValue) {
        return parseFloat(fieldValue) >= 0 && parseFloat(fieldValue) <= 100;
    }

    var exist = function(fieldValue, url, id) {
        var valid = false;
        var queryUrl = url +'/exist/' + id;
        $.ajax({
            url : queryUrl,
            async: false,
            dataType: 'json',
            success: function(response) {
                valid = !response;
            },
            error: function(response) {
                console.log('ERROR:');
                console.log('response: ', response);
            }
        })
        return valid;
    }

    function isNumeric(fieldValue) {
      return !isNaN(parseFloat(fieldValue)) && isFinite(fieldValue);
    }

    var getNestedProperty = function (obj, key) {
      // Get property array from key string
      var properties = key.split(".");

      // Iterate through properties, returning undefined if object is null or property doesn't exist
      for (var i = 0; i < properties.length; i++) {
        if (!obj || !obj.hasOwnProperty(properties[i])) {
          return;
        }
        obj = obj[properties[i]];
      }

      // Nested property found, so return the value
      return obj;
    }

    var validator = function(entityValidatorInstance, entity) {
        var errorMessages = [];

        var propertyValidator = entityValidatorInstance.propertyValidator;

        for (campo in propertyValidator) {

            /* se trata de una validación de requerido */
            if (typeof propertyValidator[campo] == "string") {
                // console.log('entity: ', entity);
                // console.log('campo: ', campo);
                // console.log('this.getNestedProperty(entity, campo): ', this.getNestedProperty(entity, campo));
                // if (entity[campo].trim() == "") {
                var propertyValue = this.getNestedProperty(entity, campo);
                if (propertyValue) {
                    if (propertyValue.toString().trim() == "") {
                        errorMessages.push(propertyValidator[campo]);
                    }
                }
                else
                    errorMessages.push(propertyValidator[campo]);
            }
            else {
                /* sino se trata de multiples validaciones para el mismo campo */
                var valid;
                for (validType in propertyValidator[campo]) {
                    if (propertyValidator[campo].hasOwnProperty(validType)) {
                        if (validType == 'customValidator') {
                            valid = propertyValidator[campo][validType]['customValidator'](entity);
                            if (!valid)
                                errorMessages.push(propertyValidator[campo][validType].errorMessage);
                        }
                        else {
                            valid = this.validate(validType, this.getNestedProperty(entity, campo),
                                propertyValidator[campo][validType].url, propertyValidator[campo][validType].id);
                            if (!valid) {
                                errorMessages.push(propertyValidator[campo][validType].errorMessage);
                                break;
                            }
                        }
                    }
                }
            }

        }
        if (entityValidatorInstance.customValidator) {
            // console.log('entity: ', entity);
            var messages = entityValidatorInstance.customValidator(entity);
            if (messages) errorMessages.push(messages);
        }

        if (errorMessages.length == 0) return true;
        return errorMessages;
    }
    var validate = function(validType, fieldValue, url, id) {
        return this[validType](fieldValue, url, id);
    }

    var validar = function(entityValidatorInstance, modelJSON) {
        // console.log('modelJSON: ', modelJSON);
        return this.validator(entityValidatorInstance, modelJSON);
    }

    return {
        showErrorMessages : showErrorMessages,
        requerido : requerido,
        fechaValida : fechaValida,
        isEmail : isEmail,
        valid_percentage : valid_percentage,
        exist : exist,
        isNumeric : isNumeric,
        validator : validator,
        validate: validate,
        validar: validar,
        getNestedProperty: getNestedProperty,
    }
})