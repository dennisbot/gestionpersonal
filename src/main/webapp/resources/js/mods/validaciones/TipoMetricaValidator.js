define([
    ], function() {

    var TipoMetricaValidator = function(modelInstance) {

        this.propertyValidator = {
            'descripcionTipoMetrica': {
                'requerido' : {
                    'errorMessage' : 'el Campo "DESCRIPCIÓN DEL TIPO DE KPI" es REQUERIDO',
                    'modelInstance': null,
                },
            },
        }
    }

    return TipoMetricaValidator;
});