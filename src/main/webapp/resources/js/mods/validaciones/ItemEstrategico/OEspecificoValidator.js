define([], function() {

    var OEspecificoValidator = function() {
        // console.log('se ha creado una instancia de OEspecificoValidator');
        this.propertyValidator = {
            'descripcionItem': 'el Campo "DESCRIPCIÓN DEL OBJETIVO ESPECÍFICO" es REQUERIDO',
            'jerarquiaOrganizacional.idJerarquiaOrganizacional': 'el Campo "GERENCIA" es REQUERIDO',
        }
        this.customValidator = function(itemEstrategicoModel) {
            var responsables = itemEstrategicoModel.responsables;
            if (responsables.length == 0)
                return "necesita seleccionar al menos un responsable";
            return false;
        }
    }

    return OEspecificoValidator;
});