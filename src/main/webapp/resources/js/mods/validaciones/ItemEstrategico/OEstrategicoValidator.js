define([], function() {

    var OEstrategicoValidator = function() {
        // console.log('se ha creado una instancia de OEstrategicoValidator');
        this.propertyValidator = {
            'descripcionItem': 'el Campo "DESCRIPCIÓN DEL OBJETIVO ESTRATÉGICO" es REQUERIDO',
        }
        this.customValidator = function(itemEstrategicoModel) {
            var responsables = itemEstrategicoModel.responsables;
            if (responsables.length == 0)
                return "necesita seleccionar al menos un responsable";
            return false;
        }
    }

    return OEstrategicoValidator;
});