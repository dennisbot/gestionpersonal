define([], function() {

    var IniciativaValidator = function() {
        // console.log('se ha creado una instancia de IniciativaValidator');

        this.propertyValidator = {
            'descripcionItem': 'El Campo "DESCRIPCIÓN DE LA INICIATIVA" es REQUERIDO',
            'jerarquiaOrganizacional.idJerarquiaOrganizacional': 'El Campo "SUPERINTENDENCIA" es REQUERIDO',
            'categoria.idCategoria': 'El Campo "CATEGORIA" es REQUERIDO',
            'metrica.idMetrica': 'Debe seleccionar un KPI',
            'tolerancia': {
                'valid_percentage' : {
                    'errorMessage' : 'Debe elegir un porcentaje válido para el campo tolerancia'
                }
            }
        }

        this.customValidator = function (itemEstrategicoModel) {
            var metas = itemEstrategicoModel.itemEstrategicos;
            if (metas.length == 0) return "necesita añadir al menos una meta";
            for (var i = 0; i < metas.length; i++) {
                if (metas[i].get('valorPlaneado').toString().trim() == "") {
                    $('#item-estrategico-' + metas[i].get('idItemEstrategico') + ' input').focus();
                    return "necesita añadir valores a cada meta, revíselas por favor";
                }
            }
            var responsables = itemEstrategicoModel.responsables;
            if (responsables.length == 0)
                return "necesita seleccionar al menos un responsable";
            return false;
        }
    }

    return IniciativaValidator;
});