define([], function() {

    var DependenciaValidator = function() {
        // console.log('se ha creado una instancia de DependenciaValidator');

        this.propertyValidator = {
            'codDependencia': {
                'requerido' : {
                    'errorMessage' : 'El Campo "Código de Dependencia" es REQUERIDO',
                },
                'customValidator' : {
                    customValidator : function(entity) {
                        if (entity.idDependenciaOrganizacional) return true;
                        var existe = false;
                        $.ajax({
                            url : 'rest/dependenciaOrganizacional/existeCodDependencia/' + entity.codDependencia,
                            async: false,
                            success: function(response) {
                                console.log('response: ', response);
                                existe = response;
                            },
                            error : function(response) {
                                console.log('response: ', response);
                                $.notify('ERROR conectándose con el servidor', 'error');
                            }
                        })
                        return !existe;
                    },
                    'errorMessage': 'El código de la dependencia ya existe, eliga otro'
                }
            },
            'nivelJerarquico.idNivelJerarquico': 'El Campo "Nivel Jerárquico" es REQUERIDO',
            'descripcionDependencia': 'El campo "Descripción de la dependencia" es REQUERIDO'
        }
    }

    return DependenciaValidator;
});