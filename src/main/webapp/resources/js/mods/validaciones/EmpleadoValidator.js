define([], function() {

    var EmpleadoValidator = function() {
        // console.log('se ha creado una instancia de EmpleadoValidator');

        this.propertyValidator = {
            'nombres': 'El Campo "Nombres" es REQUERIDO',
            'paterno': 'El Campo "APaterno" es REQUERIDO',
            'usuario': 'El campo "Usuario" es REQUERIDO',
            'nroDocumento': 'El campo "Nro de Documento" es REQUERIDO',
            'idRol': 'Debe elegir un rol para este usuario',
            'correo': {
                'requerido' : {
                    'errorMessage' : 'el Campo "eMail" es REQUERIDO',
                },
                'isEmail' : {
                    'errorMessage' : 'el Campo "eMail" no tiene un email válido',
                }
            },
            'idRh': 'El campo "ID RH" es REQUERIDO',
            'fechaIngreso': {
                'fechaValida' : {
                    'errorMessage' : 'el campo "Fecha de Ingreso" no contiene una fecha válida',
                }
            }
        }
    }

    return EmpleadoValidator;
});