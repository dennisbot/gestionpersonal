define([
    ], function() {

    var MetricaValidator = function(modelInstance) {

        this.propertyValidator = {
            'unidadMedida.idUnidadMedida': {
                'requerido' : {
                    'errorMessage' : 'debe seleccionar una UNIDAD DE MEDIDA válida',
                    'modelInstance' : null,
                }
            },
            'tipoMetrica.idTipoMetrica': {
                'requerido' : {
                    'errorMessage' : 'debe seleccionar un TIPO DE KPI válida',
                    'modelInstance' : null,
                }
            },
            'descripcionMetrica': {
                'requerido' : {
                    'errorMessage' : 'el Campo "DESCRIPCIÓN DEL KPI" es REQUERIDO',
                    'modelInstance': null,
                },
            },
            'definicionExplicacion': {
                'requerido' : {
                    'errorMessage' : 'el Campo "DEFINICIÓN Y/O EXPLICACIÓN DEL KPI" es REQUERIDO',
                    'modelInstance' : null,
                }
            },
        }
    }

    return MetricaValidator;
});