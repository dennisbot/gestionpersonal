define([
    ], function() {

    var UnidadMedidaValidator = function(modelInstance) {

        this.propertyValidator = {
            'unidad': {
                'requerido' : {
                    'errorMessage' : 'el Campo "UNIDAD DE MEDIDA" es REQUERIDO',
                    'modelInstance': null,
                },
            },
            /*'abreviatura': {
                'requerido' : {
                    'errorMessage' : 'el Campo "ABREVIATURA" es REQUERIDO',
                    'modelInstance' : null,
                }
            },*/
        }
    }

    return UnidadMedidaValidator;
});