<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%><!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"> -->

<link rel="apple-touch-icon" sizes="57x57" href="<c:url value="/static/favicon/apple-icon-57x57.png"/>">
<link rel="apple-touch-icon" sizes="60x60" href="<c:url value="/static/favicon/apple-icon-60x60.png"/>">
<link rel="apple-touch-icon" sizes="72x72" href="<c:url value="/static/favicon/apple-icon-72x72.png"/>">
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/static/favicon/apple-icon-76x76.png"/>">
<link rel="apple-touch-icon" sizes="114x114" href="<c:url value="/static/favicon/apple-icon-114x114.png"/>">
<link rel="apple-touch-icon" sizes="120x120" href="<c:url value="/static/favicon/apple-icon-120x120.png"/>">
<link rel="apple-touch-icon" sizes="144x144" href="<c:url value="/static/favicon/apple-icon-144x144.png"/>">
<link rel="apple-touch-icon" sizes="152 x152" href="<c:url value="/static/favicon/apple-icon-152x152.png"/>">
<link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/static/favicon/apple-icon-180x180.png"/>">
<link rel="icon" type="image/png" sizes="192x192" href="<c:url value="/static/favicon/android-icon-192x192.png"/>">
<link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/static/favicon/favicon-32x32.png"/>">
<link rel="icon" type="image/png" sizes="96x96" href="<c:url value="/static/favicon/favicon-96x96.png"/>">
<link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/static/favicon/favicon-16x16.png"/>">
<link rel="manifest" href="<c:url value="/static/favicon/manifest.json"/>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/static/favicon/favicon.ico"/>">

<link rel="stylesheet" href="<c:url value="/static/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet" href="<c:url value="/static/css/login.css"/>">
<link rel="stylesheet" href="<c:url value="/static/css/font-awesome.min.css"/>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="<c:url value="/static/js/plugins/misc/html5shiv.js"/>"></script>
   <script src="<c:url value="/static/js/plugins/misc/respond.min.js"/>"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body role="document" class="login">
    <div class="row vertical-center" id="contenedorLogin">
      <div class="col-md-4 col-md-offset-4 border-left" id="formLogin">
        <form:form commandName="loginUser" id="form-action">
          <div class="logo">
            <div><img src="<c:url value="/static/css/img/logolbv2.png"/>"></div>
            <strong style="text-transform: uppercase;font-size: 16px;color: #C51230;">${initParam.APP_NAME} <small>V${initParam.APP_VERSION}</small></strong>
          </div>
          <div class="form-group">
              <div class="input-group">
                  <form:input path="usuario" class="form-control" id="usuario" placeholder="usuario" value="${(not empty sessionScope.usuario) ? sessionScope.usuario : 'dennisbot'}" autofocus="${(empty sessionScope.usuario) ? 'autofocus' : ''}" /><span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
          </div>
          <div class="form-group">
              <div class="input-group">
                <form:password  path="password" class="form-control" id="password" placeholder="contraseña" value="holamundo" autofocus="autofocus" /><span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
          </div>
          <div class="form-group">
              <div class="input-group">
                  <button class="btn btn-primary" id="iniciar-sesion"><span class="glyphicon glyphicon-ok"></span> INICIAR SESIÓN </button>
              </div>
          </div>
          <c:if test="${not empty error}">
          <div class="form-group alert alert-danger">
            <c:out value="${error}" />
          </div>
          </c:if>
        </form:form>
      </div>
    </div>
    <script src="<c:url value="/static/js/lib/jquery.min.js"/>"></script>
    <script src="<c:url value="/static/js/login/login.js"/>"></script>
</body>
</html>