<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Copyright &copy; 2014-2015 http://almsaeedstudio.com All rights reserved. Version 2.3.0 -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="<c:url value="/static/favicon/apple-icon-57x57.png"/>">
    <link rel="apple-touch-icon" sizes="60x60" href="<c:url value="/static/favicon/apple-icon-60x60.png"/>">
    <link rel="apple-touch-icon" sizes="72x72" href="<c:url value="/static/favicon/apple-icon-72x72.png"/>">
    <link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/static/favicon/apple-icon-76x76.png"/>">
    <link rel="apple-touch-icon" sizes="114x114" href="<c:url value="/static/favicon/apple-icon-114x114.png"/>">
    <link rel="apple-touch-icon" sizes="120x120" href="<c:url value="/static/favicon/apple-icon-120x120.png"/>">
    <link rel="apple-touch-icon" sizes="144x144" href="<c:url value="/static/favicon/apple-icon-144x144.png"/>">
    <link rel="apple-touch-icon" sizes="152x152" href="<c:url value="/static/favicon/apple-icon-152x152.png"/>">
    <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/static/favicon/apple-icon-180x180.png"/>">
    <link rel="icon" type="image/png" sizes="192x192" href="<c:url value="/static/favicon/android-icon-192x192.png"/>">
    <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/static/favicon/favicon-32x32.png"/>">
    <link rel="icon" type="image/png" sizes="96x96" href="<c:url value="/static/favicon/favicon-96x96.png"/>">
    <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/static/favicon/favicon-16x16.png"/>">
    <link rel="manifest" href="<c:url value="/static/favicon/manifest.json"/>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<c:url value="/static/favicon/ms-icon-144x144.png"/>">
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/static/favicon/favicon.ico"/>">

    <title>${initParam.APP_NAME} | LAS BAMBAS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<c:url value="/static/bootstrap/css/bootstrap.css" />">
    <link rel="stylesheet" href="<c:url value="/static/css/chosen/bootstrap-chosen.css" />">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<c:url value="/static/css/font-awesome.min.css" />">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<c:url value="/static/css/ionicons.min.css" />">
    <!-- Theme style -->
    <link rel="stylesheet" href="<c:url value="/static/css/AdminLTE.min.css" />">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<c:url value="/static/css/skins/_all-skins.min.css" />">
    <link rel="stylesheet" href="<c:url value="/static/css/gpersonal.css" />">
    <!-- iCheck -->
    <link rel="stylesheet" href="<c:url value="/static/plugins/iCheck/flat/blue.css" />">
    <!-- para el datatables -->
    <link rel="stylesheet" href="<c:url value="/static/DataTables-1.10.9/media/css/dataTables.bootstrap.css"/>">
    <link rel="stylesheet" href="<c:url value="/static/file-uploader/fineuploader.css"/>">
    <link rel="stylesheet" href="<c:url value="/static/css/datepicker3.css"/>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	   <script src="<c:url value="/static/js/plugins/misc/html5shiv.js"/>"></script>
	   <script src="<c:url value="/static/js/plugins/misc/respond.min.js"/>"></script>
	<![endif]-->
  <!-- custom css -->
  <style id="custom-css"></style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<c:url value="/"/>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>G</b>PR</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>G</b>Personal <small>v${initParam.APP_VERSION}</small></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <c:if test="${not empty usuario.uuidFoto}">
                  <img src="<c:url value="/uploadFoto/${usuario.uuidFoto}"/>" class="user-image loadimage" alt="User Image">
                </c:if>
                <c:if test="${empty usuario.uuidFoto}">
                    <img src="<c:url value="/static/img/fotos/male_placeholder.jpg"/>" class="user-image loadimage" alt="User Image">
                </c:if>
                  <span class="hidden-xs"><span id="usuario-small"></span></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <c:if test="${not empty usuario.uuidFoto}">
                  <img src="<c:url value="/uploadFoto/${usuario.uuidFoto}"/>" class="img-circle loadimage" alt="User Image">
                </c:if>
                <c:if test="${empty usuario.uuidFoto}">
                    <img src="<c:url value="/static/img/fotos/male_placeholder.jpg"/>" class="img-circle loadimage" alt="User Image">
                </c:if>
                    <p>
                        <span id="usuario-perfil"></span><br>
                        <span><small id="usuario-cargo"></small></span>
                      <small>Miembro desde Abr. 2015</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#mi-perfil" class="btn btn-primary">Perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="<c:url value="/logout"/>" class="btn btn-primary">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
               <c:if test="${not empty usuario.uuidFoto}">
                  <img src="<c:url value="/uploadFoto/${usuario.uuidFoto}"/>" class="img-circle loadimage" alt="User Image">
                </c:if>
               <c:if test="${empty usuario.uuidFoto}">
              <img src="<c:url value="/static/img/fotos/male_placeholder.jpg"/>" class="img-circle loadimage" alt="User Image">
                </c:if>
            </div>
            <div class="pull-left info">
              <p><span id="usuario-sidebar"></span></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">PRINCIPAL</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#nivel-jerarquico"><i class="fa fa-circle-o"></i>Niveles Jer�rquicos</a></li>
                <li><a href="#dependencia-organizacional"><i class="fa fa-circle-o"></i> Dependencias Orgs.</a></li>
                <li><a href="#cargo/<c:out value="${pathMiDependencia}" />"><i class="fa fa-circle-o"></i> Mi dependencia</a></li>
                <li><a href="#empleado"><i class="fa fa-circle-o"></i> Registrar Personal</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div id="container">
          <section class="content-header" id="content-header">
              <h1 class="text-center">SISTEMA DE CONTROL DE PERSONAL</h1>
          </section>
          <!-- Main content -->
          <section class="content" id="content">
            <h4 class="text-center"><i class="fa fa-refresh fa-spin"></i> Cargando...</h4>
            <p class="text-center"><img src="static/img/organigrama/culture_organizational.jpg" class="imagen-fondo" alt=""></p>
          </section><!-- /.content -->
        </div>
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> ${initParam.APP_VERSION}
        </div>
        <strong>Gesti�n de Personal
      </footer>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <div id="modal-area"></div>
    <script>
      var BASE_URL = '<c:out value="${initParam['BASE_URL']}" />'
    </script>
    <!-- <script src="<c:url value="/static/DataTables-1.10.9/media/js/jquery.js" />"></script>
    <script src="<c:url value="/static/js/plugins/typeahead/typeahead.bundle.min.js" />"></script> -->
    <script data-main="static/js/main" src="<c:url value="/static/js/lib/require.js" />"></script>
    <script src="<c:url value="/static/js/plugins/fuzzy/fuzzy.js" />"></script>
    <script src="<c:url value="/static/file-uploader/fineuploader.js" />"></script>
    <!-- FastClick -->
    <!--[if gte IE 11]>
    <script src="<c:url value="/static/plugins/fastclick/fastclick.min.js"/>"></script>
    <![endif]-->
  </body>
</html>
