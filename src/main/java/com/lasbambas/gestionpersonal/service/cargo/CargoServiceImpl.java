package com.lasbambas.gestionpersonal.service.cargo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lasbambas.gestionpersonal.dao.cargo.CargoDao;
import com.lasbambas.gestionpersonal.model.Cargo;

@Service
@Transactional
public class CargoServiceImpl implements CargoService {
	@Autowired
	private CargoDao cargoDao;

	public List<Cargo> findAll() {
		return cargoDao.findAll();
	}

	public Cargo findById(int idCargo) {
		return cargoDao.findById(idCargo);
	}
	
	public List<Cargo> findByIdDependencia(int idDependencia) {
		return cargoDao.findByIdDependencia(idDependencia);
	}
	
	public Cargo findByNivel(int nivel) {
		// TODO Auto-generated method stub
		return null;
	}

	public Cargo save(Cargo cargo) {
		// TODO Auto-generated method stub
		return null;
	}

	public Cargo create(Cargo cargo) {
		return cargoDao.create(cargo);
	}

	public Cargo update(Cargo cargo) {
		return cargoDao.update(cargo);
	}

	public boolean remove(int idCargo) {
		return cargoDao.remove(idCargo);
	}

	public List<Cargo> getLessThanEqualNivel(int nivel) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
