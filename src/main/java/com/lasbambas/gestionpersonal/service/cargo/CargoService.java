package com.lasbambas.gestionpersonal.service.cargo;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;

public interface CargoService {
	public List<Cargo> findAll();
	public Cargo findById(int idCargo);
	public List<Cargo> findByIdDependencia(int idDependencia);
	public Cargo findByNivel(int nivel);
	public Cargo save(Cargo cargo);
	public Cargo create(Cargo cargo);
	public Cargo update(Cargo cargo);
	public boolean remove(int idCargo);
	public List<Cargo> getLessThanEqualNivel(int nivel);
}
