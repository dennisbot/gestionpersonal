package com.lasbambas.gestionpersonal.service.nivelJerarquico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lasbambas.gestionpersonal.dao.nivelJerarquico.NivelJerarquicoDao;
import com.lasbambas.gestionpersonal.model.NivelJerarquico;

@Service
@Transactional
public class NivelJerarquicoServiceImpl implements NivelJerarquicoService {
	@Autowired
	private NivelJerarquicoDao nivelJerarquicoDao;
	public List<NivelJerarquico> findAll() {
		return nivelJerarquicoDao.findAll();
	}
	public NivelJerarquico findById(int idNivelJerarquico) {
		return nivelJerarquicoDao.findById(idNivelJerarquico);
	}
	public NivelJerarquico findByNivel(int nivel) {
		return nivelJerarquicoDao.findByNivel(nivel);
	}

	public List<NivelJerarquico> getLessThanEqualNivel(int nivel) {
		return nivelJerarquicoDao.getLessThanEqualNivel(nivel);
	}

	public NivelJerarquico save(NivelJerarquico nivelJerarquico) {
		System.out.println(nivelJerarquico);
		// TODO Auto-generated method stub
		return null;
	}
	public NivelJerarquico create(NivelJerarquico nivelJerarquico) {
		System.out.println("create was called");
		System.out.println(nivelJerarquico);
		// TODO Auto-generated method stub
		return null;
	}
	public NivelJerarquico update(NivelJerarquico nivelJerarquico) {
		System.out.println("update was called");
		System.out.println(nivelJerarquico);
		return nivelJerarquicoDao.update(nivelJerarquico);
	}
	public boolean remove(int idNivelJerarquico) {
		// TODO Auto-generated method stub
		return false;
	}
}
