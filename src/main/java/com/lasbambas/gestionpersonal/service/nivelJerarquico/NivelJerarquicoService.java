package com.lasbambas.gestionpersonal.service.nivelJerarquico;

import java.util.List;

import com.lasbambas.gestionpersonal.model.NivelJerarquico;

public interface NivelJerarquicoService {
	public List<NivelJerarquico> findAll();
	public NivelJerarquico findById(int idNivelJerarquico);
	public NivelJerarquico findByNivel(int nivel);
	public NivelJerarquico save(NivelJerarquico nivelJerarquico);
	public NivelJerarquico create(NivelJerarquico nivelJerarquico);
	public NivelJerarquico update(NivelJerarquico nivelJerarquico);
	public boolean remove(int idNivelJerarquico);
	public List<NivelJerarquico> getLessThanEqualNivel(int nivel);
}
