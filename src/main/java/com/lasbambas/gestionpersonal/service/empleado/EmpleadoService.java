package com.lasbambas.gestionpersonal.service.empleado;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;

public interface EmpleadoService {
	public Empleado canLogIn(Empleado empleado) throws Exception;
	public List<Empleado> findAll();
	public Empleado findById(int idEmpleado);
	public List<EmpleadoWrapperSelect> toSelect();
	public Empleado save(Empleado empleado);
	public Empleado create(Empleado empleado)  throws Exception;
	public Empleado update(Empleado empleado);
	public boolean remove(int idEmpleado);
	public void changePassword(Empleado empleado);
}
