package com.lasbambas.gestionpersonal.service.empleado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lasbambas.gestionpersonal.dao.empleado.EmpleadoDao;
import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;

@Service
@Transactional
public class EmpleadoServiceImpl implements EmpleadoService {
	@Autowired
	private EmpleadoDao empleadoDao;
	public Empleado canLogIn(Empleado empleado) throws Exception {
		return empleadoDao.canLogIn(empleado);
	}
	public List<Empleado> findAll() {
		return empleadoDao.findAll();
	}
	public Empleado findById(int idEmpleado) {
		return empleadoDao.findById(idEmpleado);
	}
	public List<EmpleadoWrapperSelect> toSelect() {
		return empleadoDao.toSelect();
	}
	public Empleado save(Empleado empleado) {
		// TODO Auto-generated method stub
		return null;
	}
	public Empleado create(Empleado empleado) throws Exception {
		return empleadoDao.create(empleado);
	}
	public Empleado update(Empleado empleado) {
		return empleadoDao.update(empleado);
	}
	public boolean remove(int idEmpleado) {
		// TODO Auto-generated method stub
		return false;
	}
	public void changePassword(Empleado empleado) {
		this.update(empleado);
	}
}
