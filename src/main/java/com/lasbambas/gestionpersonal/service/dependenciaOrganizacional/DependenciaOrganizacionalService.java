package com.lasbambas.gestionpersonal.service.dependenciaOrganizacional;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;

public interface DependenciaOrganizacionalService {
	public List<DependenciaOrganizacional> findAll();
	public DependenciaOrganizacional findById(int idDependenciaOrganizacional);
	public List<DependenciaOrganizacional> findByNivel(int nivel, int idDependencia);
	public List<DependenciaOrganizacional> getHierachy(int idDependenciaOrganizacional);
	public DependenciaOrganizacional save(DependenciaOrganizacional dependenciaOrganizacional);
	public DependenciaOrganizacional create(DependenciaOrganizacional dependenciaOrganizacional);
	public DependenciaOrganizacional update(DependenciaOrganizacional dependenciaOrganizacional);
	public boolean remove(int idDependenciaOrganizacional);
	public boolean existeCodDependencia(String codDependencia);
	public List<DependenciaOrganizacional> findOneLevelBackDeps(int nivel);
	public List<DependenciaOrganizacional> findDepsBetweenLevels(int loLvl, int hiLvl);
	public String nextCodDependencia();
}

