package com.lasbambas.gestionpersonal.service.dependenciaOrganizacional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lasbambas.gestionpersonal.dao.dependenciaOrganizacional.DependenciaOrganizacionalDao;
import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;

@Service
@Transactional
public class DependenciaOrganizacionalServiceImpl implements DependenciaOrganizacionalService {
	@Autowired
	private DependenciaOrganizacionalDao dependenciaOrganizacionalDao;

	public List<DependenciaOrganizacional> findAll() {
		return dependenciaOrganizacionalDao.findAll();
	}
	public DependenciaOrganizacional findById(int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalDao.findById(idDependenciaOrganizacional);
	}
	public List<DependenciaOrganizacional> findByNivel(int nivel, int idDependencia) {
		return dependenciaOrganizacionalDao.findByNivel(nivel, idDependencia);
	}
	
	public List<DependenciaOrganizacional> getHierachy(int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalDao.getHierachy(idDependenciaOrganizacional);
	}
	
	public DependenciaOrganizacional save(DependenciaOrganizacional dependenciaOrganizacional) {
		System.out.println(dependenciaOrganizacional);
		return dependenciaOrganizacionalDao.save(dependenciaOrganizacional);
	}
	public DependenciaOrganizacional create(DependenciaOrganizacional dependenciaOrganizacional) {
		return dependenciaOrganizacionalDao.create(dependenciaOrganizacional);
	}
	public DependenciaOrganizacional update(DependenciaOrganizacional dependenciaOrganizacional) {
		return dependenciaOrganizacionalDao.update(dependenciaOrganizacional);
	}
	public boolean remove(int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalDao.remove(idDependenciaOrganizacional);
	}
	@Override
	public boolean existeCodDependencia(String codDependencia) {
		return dependenciaOrganizacionalDao.existeCodDependencia(codDependencia);
	}
	@Override
	public List<DependenciaOrganizacional> findOneLevelBackDeps(int nivel) {
		return dependenciaOrganizacionalDao.findOneLevelBackDeps(nivel);
	}
	@Override
	public List<DependenciaOrganizacional> findDepsBetweenLevels(int loLvl, int hiLvl) {
		return dependenciaOrganizacionalDao.findDepsBetweenLevels(loLvl, hiLvl);
	}
	@Override
	public String nextCodDependencia() {
		return dependenciaOrganizacionalDao.nextCodDependencia();
	}
}
