/* MiddlewareUtil - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.gestionpersonal.utils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.CRC32;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FilenameUtils;

import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeUtility;

public class MiddlewareUtil
{
    public static Date cadenaAFecha(String fecha, String formato)
	throws Exception {
	Date date;
	try {
	    SimpleDateFormat sdf = new SimpleDateFormat(formato);
	    date = sdf.parse(fecha);
	} catch (Exception e) {
	    throw e;
	}
	return date;
    }
    
    public static String fechaACadena(Date fecha, String formato)
	throws Exception {
	String string;
	try {
	    Locale locale = new Locale("es", "PE");
	    SimpleDateFormat sdf = new SimpleDateFormat(formato, locale);
	    string = sdf.format(fecha);
	} catch (Exception e) {
	    throw e;
	}
	return string;
    }
    
    public static String encriptaPassword(String password) throws Exception {
		StringBuffer hashStringBuffer = null;
		try {
		    MessageDigest md = MessageDigest.getInstance("SHA1");
		    md.update(password.getBytes());
		    byte[] hash = md.digest();
		    hashStringBuffer = new StringBuffer();
		    for (int index = 0; index < hash.length; index++) {
			String byteString = String.valueOf(hash[index] + 128);
			int byteLength = byteString.length();
			switch (byteLength) {
			case 1:
			    byteString = new StringBuilder("00").append(byteString)
					     .toString();
			    break;
			case 2:
			    byteString
				= new StringBuilder("0").append(byteString).toString();
			    break;
			}
			hashStringBuffer.append(byteString);
		    }
		} catch (Exception e) {
		    throw e;
		}
		String pass = hashStringBuffer.toString().substring(0, 30);
		return pass;
    }
    public static void main(String[] args) throws Exception {
//    	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//    	EmpleadoDao empleadoDao =  (EmpleadoDao)context.getBean("empleadoDao");
//    	List<EmpleadoDTO> empleados = empleadoDao.findAll();
//    	for (EmpleadoDTO empleado : empleados) {
//    		empleado.setPassword(MiddlewareUtil.encriptaPassword(empleado.getPassword()));
//    		empleadoDao.save(empleado);
//    	}
//    	System.out.println("se actualizó las contraseñas de todos los empleados");
	}
    public static String generarCRC(File archivo) throws Exception {
	String crc = "";
	try {
	    java.util.zip.Checksum checksum = new CRC32();
	    FileInputStream fis = new FileInputStream(archivo);
	    byte[] dataBytes = fis.toString().getBytes();
	    checksum.update(dataBytes, 0, dataBytes.length);
	    crc = String.valueOf(checksum.getValue());
	} catch (Exception e) {
	    throw e;
	}
	return crc;
    }
    
    public static String generarChecksum(File archivo) throws Exception {
	String checksum = "";
	try {
	    MessageDigest md = MessageDigest.getInstance("SHA1");
	    FileInputStream fis = new FileInputStream(archivo);
	    byte[] dataBytes = new byte[1024];
	    int nread = 0;
	    while ((nread = fis.read(dataBytes)) != -1)
		md.update(dataBytes, 0, nread);
	    byte[] mdbytes = md.digest();
	    StringBuffer sb = new StringBuffer("");
	    for (int i = 0; i < mdbytes.length; i++)
		sb.append(Integer.toString((mdbytes[i] & 0xff) + 256, 16)
			      .substring(1));
	    checksum = sb.toString();
	} catch (Exception e) {
	    throw e;
	}
	return checksum;
    }
    
    public static String obtenerNombreArchivo(String nombreCompleto)
	throws Exception {
	String nombres = "";
	int pos = nombreCompleto.lastIndexOf(".");
	if (pos > -1)
	    nombres = nombreCompleto.substring(0, pos);
	else
	    nombres = nombreCompleto;
	return nombres;
    }
    
    public static String obtenerRutaArchivo(String nombreCompleto)
	throws Exception {
	String ruta
	    = new StringBuilder(FilenameUtils.getPrefix(nombreCompleto)).append
		  (FilenameUtils.getPath(nombreCompleto)).toString();
	return ruta;
    }
    
    public static Date restarFecha(int dias) {
	Date hoy = new Date();
	Date fecha = null;
	long valor = hoy.getTime() - (long) dias * 86400000L;
	fecha = new Date(valor);
	return fecha;
    }
    
    public static FileLock bloquearArchivo(File archivoBloquear)
	throws OverlappingFileLockException, IOException {
	RandomAccessFile raf = new RandomAccessFile(archivoBloquear, "rw");
	FileChannel fc = raf.getChannel();
	FileLock l = fc.tryLock();
	return l;
    }
    
    public static void copiarArchivos(File tempFile, File finalFile)
	throws Exception {
	FileInputStream fis = new FileInputStream(tempFile);
	FileOutputStream fos = new FileOutputStream(finalFile);
	FileChannel s = fis.getChannel();
	FileChannel d = fos.getChannel();
	if (d != null && s != null)
	    d.transferFrom(s, 0L, s.size());
	if (s != null) {
	    s.close();
	    fos.close();
	}
	if (d != null) {
	    d.close();
	    fis.close();
	}
    }
    
    private static byte[] encode(byte[] b) throws Exception {
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	OutputStream b64os = MimeUtility.encode(baos, "base64");
	b64os.write(b);
	b64os.close();
	return baos.toByteArray();
    }
    
    private static byte[] decode(byte[] b) throws Exception {
	ByteArrayInputStream bais = new ByteArrayInputStream(b);
	InputStream b64is = MimeUtility.decode(bais, "base64");
	byte[] tmp = new byte[b.length];
	int n = b64is.read(tmp);
	byte[] res = new byte[n];
	System.arraycopy(tmp, 0, res, 0, n);
	return res;
    }
    
    private static SecretKeySpec getKey() {
	SecretKeySpec key = new SecretKeySpec("Dispatch".getBytes(), "DES");
	return key;
    }
    
    public static String desencripta(String s) throws Exception {
	String s1 = null;
	if (s.indexOf("{DES}") != -1) {
	    String s2 = s.substring("{DES}".length());
	    Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	    cipher.init(2, getKey());
	    byte[] abyte0 = cipher.doFinal(decode(s2.getBytes()));
	    s1 = new String(abyte0);
	} else
	    s1 = s;
	return s1;
    }
    
    public static String encripta(String s) throws Exception {
	SecureRandom securerandom = new SecureRandom();
	securerandom.nextBytes("Dispatch".getBytes());
	Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	cipher.init(1, getKey());
	byte[] abyte0 = encode(cipher.doFinal(s.getBytes()));
	return new StringBuilder("{DES}").append(new String(abyte0))
		   .toString();
    }
}
