package com.lasbambas.gestionpersonal.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate3.Hibernate3Module;

@SuppressWarnings("serial")
public class HibernateAwareObjectMapper extends ObjectMapper {
	 public HibernateAwareObjectMapper() {
	        registerModule(new Hibernate3Module());
	        // habilitar las 3 lineas de abajo en producción (para no pasar valores nulos)
//	        this.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
//	        this.setSerializationInclusion(Include.NON_NULL);
//	        this.setSerializationInclusion(Include.NON_EMPTY);
//	        this.setSerializationInclusion(Include. non_default?);
//	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	        setDateFormat(df);
	    }
}
