package com.lasbambas.gestionpersonal.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.service.cargo.CargoService;
import com.lasbambas.gestionpersonal.service.empleado.EmpleadoService;

@Controller
public class LoginController {
	@Autowired
	private EmpleadoService empleadoService;
	@Autowired
	private CargoService cargoService;

	@RequestMapping("/")
	public String main(HttpSession session) {
		if (session.getAttribute("usuario") == null)
			return "redirect:/login";

//		System.out.println("entró al controlador main root /");
		return "main";
	}

	@RequestMapping("/login")
	public String login(Model model, HttpSession session) {

		if (session.getAttribute("usuario") != null)
			return "redirect:/";

		model.addAttribute("loginUser", new Empleado());
		return "login";
	}
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	public String dologin(@ModelAttribute("loginUser") Empleado empleado, HttpSession session, Model model) 
			throws Exception {
		System.out.println(empleado);
		Empleado e = empleadoService.canLogIn(empleado);
		System.out.println(e);
		if (e != null) {
			if (e.getCargo() != null) {
				Cargo c = cargoService.findById(e.getCargo().getIdCargo());
				String pathMiDependencia = c.getDependenciaOrganizacional().getNivelJerarquico().getIdNivelJerarquico() + "/";
				pathMiDependencia += c.getDependenciaOrganizacional().getIdDependenciaOrganizacional();
				session.setAttribute("pathMiDependencia", pathMiDependencia);
			}
			session.setAttribute("usuario", e);
			return "redirect:/";
		}
		System.out.println("entro en método post de controller login");
		model.addAttribute("error", "usuario y/o contraseña incorrectos");
		//se puede usar ambos, pero mejor es con redirect (pero se pierde los atributos de model)
		return "login";
//		return "redirect:/login";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		// finalizar la sesión y luego redirigir a login
		session.invalidate();
		return "redirect:/login";
	}
}
