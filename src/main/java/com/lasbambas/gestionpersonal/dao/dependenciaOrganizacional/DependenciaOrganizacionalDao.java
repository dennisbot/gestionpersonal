package com.lasbambas.gestionpersonal.dao.dependenciaOrganizacional;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;

public interface DependenciaOrganizacionalDao {
	public List<DependenciaOrganizacional> findAll();
	public DependenciaOrganizacional findById(int idDependenciaOrganizacional);
	public List<DependenciaOrganizacional> findByNivel(int nivel, int idDependencia);
	public List<DependenciaOrganizacional> getLessThanEqualNivel(int nivel);
	public DependenciaOrganizacional save(DependenciaOrganizacional dependenciaOrganizacional);
	public DependenciaOrganizacional create(DependenciaOrganizacional dependenciaOrganizacional);
	public DependenciaOrganizacional update(DependenciaOrganizacional dependenciaOrganizacional);
	public boolean remove(int idDependenciaOrganizacional);
	public List<DependenciaOrganizacional> getHierachy(int idDependenciaOrganizacional);
	public List<DependenciaOrganizacional> findOneLevelBackDeps(int nivel);
	public List<DependenciaOrganizacional> findDepsBetweenLevels(int loLvl, int hiLvl);
	public boolean existeCodDependencia(String codDependencia);
	public String nextCodDependencia();
}
