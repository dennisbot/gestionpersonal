package com.lasbambas.gestionpersonal.dao.dependenciaOrganizacional;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lasbambas.gestionpersonal.dao.nivelJerarquico.NivelJerarquicoDao;
import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;
import com.lasbambas.gestionpersonal.model.NivelJerarquico;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;
import com.lasbambas.gestionpersonal.utils.queryUtil;

@Repository
public class DependenciaOrganizacionalDaoImpl implements DependenciaOrganizacionalDao{

	@PersistenceContext
	EntityManager em;
	@Autowired
	NivelJerarquicoDao nivelJerarquicoDao;

	public List<DependenciaOrganizacional> findAll() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DependenciaOrganizacional> criteria = cb.createQuery(DependenciaOrganizacional.class);
		Root<DependenciaOrganizacional> jerarquiaOrg = criteria.from(DependenciaOrganizacional.class);
		jerarquiaOrg.fetch("nivelJerarquico");
		jerarquiaOrg.fetch("dependenciaOrganizacional", JoinType.LEFT);
//		jerarquiaOrg.fetch("dependenciaOrganizacional");
		criteria.select(jerarquiaOrg);
		return em.createQuery(criteria).getResultList();

//		String sqlQuery = "SELECT do FROM dependenciaOrganizacional do";
//		TypedQuery<DependenciaOrganizacional> query = em.createQuery(sqlQuery, DependenciaOrganizacional.class);
//		return query.getResultList();
	}

	public DependenciaOrganizacional findById(int idDependenciaOrganizacional) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<DependenciaOrganizacional> criteria = cb.createQuery(DependenciaOrganizacional.class);
			Root<DependenciaOrganizacional> jerarquiaOrg = criteria.from(DependenciaOrganizacional.class);
			//añadimos el nombre de la propiedad parent que queremos añadir (porque está en lazy)
//			jerarquiaOrg.fetch("nivelJerarquico");
			jerarquiaOrg.fetch("dependenciaOrganizacional", JoinType.LEFT);
			criteria.select(jerarquiaOrg).where(
					cb.equal(jerarquiaOrg.get("idDependenciaOrganizacional"), idDependenciaOrganizacional)
			);
			return em.createQuery(criteria).getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró la dependencia Organizacional con el id = "
					+ idDependenciaOrganizacional);
			return null;
		}
	}
	public List<DependenciaOrganizacional> findByNivel(int nivel, int idDependencia) {
		try {
			String sqlExtra = "";
			if (idDependencia != 0)
				sqlExtra = " AND do.dependenciaOrganizacional.idDependenciaOrganizacional = :idDependencia";
			String sqlQuery = "SELECT do FROM DependenciaOrganizacional do " +
					"JOIN FETCH do.nivelJerarquico n  " +
					"LEFT JOIN FETCH do.dependenciaOrganizacional p " +
					"WHERE n.nivel "+ (sqlExtra.equals("") ? "=" : ">=") + ":nivel" + sqlExtra;
			TypedQuery<DependenciaOrganizacional> query = em.createQuery(sqlQuery, DependenciaOrganizacional.class);
			query.setParameter("nivel", nivel);
			if (idDependencia != 0) 
				query.setParameter("idDependencia", idDependencia);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró el nivel jerarquico con el nivel = "
					+ nivel);
			return null;
		}
	}
	
	public List<DependenciaOrganizacional> getLessThanEqualNivel(int nivel) {
		try {
			String sqlQuery = "SELECT do FROM DependenciaOrganizacional do " +
					"JOIN do.nivelJerarquico n WHERE n.nivel <= :nivel";
			TypedQuery<DependenciaOrganizacional> query = em.createQuery(sqlQuery, DependenciaOrganizacional.class);
			query.setParameter("nivel", nivel);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró la jerarquía con el nivel <= "
					+ nivel);
			return null;
		}
	}

	public DependenciaOrganizacional save(DependenciaOrganizacional dependenciaOrganizacional) {
		// TODO Auto-generated method stub
		return null;
	}

	public DependenciaOrganizacional create(DependenciaOrganizacional dependenciaOrganizacional) {		
		em.persist(dependenciaOrganizacional);
		em.flush(); //force insert to receive the id of dependenciaOrganizacional
		return dependenciaOrganizacional;
	}

	public DependenciaOrganizacional update(DependenciaOrganizacional dependenciaOrganizacional) {
		HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
		try {
			String val = mapper.writeValueAsString(dependenciaOrganizacional);
			System.out.println("dependenciaOrganizacional:");
			System.out.println(val);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return em.merge(dependenciaOrganizacional);
	}

	public boolean remove(int idDependenciaOrganizacional) {
		DependenciaOrganizacional dep = em.find(DependenciaOrganizacional.class, idDependenciaOrganizacional);
		em.remove(dep);
		return true;
	}

	public List<DependenciaOrganizacional> getHierachy(int idDependenciaOrganizacional) {
		String sql = "WITH  JERARQUIA_DEPENDENCIA_ORGANIZACIONAL " +
				"AS ( SELECT   * " +
				"FROM     GPERSONAL.DEPENDENCIA_ORGANIZACIONAL DO " +
				"WHERE    ID_DEPENDENCIA_ORGANIZACIONAL = :idDependencia " +
				"UNION ALL " +
				"SELECT   DO.* " +
				"FROM     GPERSONAL.DEPENDENCIA_ORGANIZACIONAL DO " +
				"INNER JOIN JERARQUIA_DEPENDENCIA_ORGANIZACIONAL JDO  " +
				"ON DO.ID_DEPENDENCIA_ORGANIZACIONAL = JDO.ID_PARENT_DEPENDENCIA " +
				") " +
				"SELECT  * " +
				"FROM    JERARQUIA_DEPENDENCIA_ORGANIZACIONAL";
		Query query = em.createNativeQuery(sql, DependenciaOrganizacional.class);
		query.setParameter("idDependencia", idDependenciaOrganizacional);
		return query.getResultList();
	}

	public List<DependenciaOrganizacional> findOneLevelBackDeps(int nivel) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DependenciaOrganizacional> criteria = cb.createQuery(DependenciaOrganizacional.class);
		Root<DependenciaOrganizacional> jerarquiaOrg = criteria.from(DependenciaOrganizacional.class); 
		jerarquiaOrg.fetch("nivelJerarquico");
		jerarquiaOrg.fetch("dependenciaOrganizacional", JoinType.LEFT);
//		jerarquiaOrg.fetch("dependenciaOrganizacional");
		criteria.select(jerarquiaOrg)
				.where(
						cb.equal(
								jerarquiaOrg.get("nivelJerarquico").get("idNivelJerarquico"),
								nivel - 1
						)
//						cb.lt(
//								jerarquiaOrg.get("nivelJerarquico").<Integer>get("idNivelJerarquico"),
//								nivel
//								)
//								
				);
		return em.createQuery(criteria).getResultList();
//		String sqlQuery = "SELECT do FROM dependenciaOrganizacional do";
//		TypedQuery<DependenciaOrganizacional> query = em.createQuery(sqlQuery, DependenciaOrganizacional.class);
//		return query.getResultList();
	}
	public List<DependenciaOrganizacional> findDepsBetweenLevels(int loLvl, int hiLvl) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DependenciaOrganizacional> criteria = cb.createQuery(DependenciaOrganizacional.class);
		Root<DependenciaOrganizacional> jerarquiaOrg = criteria.from(DependenciaOrganizacional.class); 
		jerarquiaOrg.fetch("nivelJerarquico");
		jerarquiaOrg.fetch("dependenciaOrganizacional", JoinType.LEFT);
//		jerarquiaOrg.fetch("dependenciaOrganizacional");
		criteria.select(jerarquiaOrg)
			.where(
				cb.greaterThanOrEqualTo(jerarquiaOrg.get("nivelJerarquico").<Integer>get("idNivelJerarquico"), loLvl),
				cb.lessThanOrEqualTo(jerarquiaOrg.get("nivelJerarquico").<Integer>get("idNivelJerarquico"), hiLvl)
			);
		return em.createQuery(criteria).getResultList();
//		String sqlQuery = "SELECT do FROM dependenciaOrganizacional do";
//		TypedQuery<DependenciaOrganizacional> query = em.createQuery(sqlQuery, DependenciaOrganizacional.class);
//		return query.getResultList();
	}

	@Override
	public boolean existeCodDependencia(String codDependencia) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DependenciaOrganizacional> criteria = cb.createQuery(DependenciaOrganizacional.class);
		Root<DependenciaOrganizacional> jerarquiaOrg = criteria.from(DependenciaOrganizacional.class);
		criteria.select(jerarquiaOrg).where(
				cb.equal(jerarquiaOrg.get("codDependencia"), codDependencia)
		);
		try {
			return em.createQuery(criteria).getSingleResult() != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String nextCodDependencia() {
		String maxCodDep = (String)em.createQuery("select MAX(d.codDependencia) "
				+ "from DependenciaOrganizacional d")
				.getSingleResult();
		return "DEP-" + String.format("%03d", Integer.valueOf(maxCodDep.substring(4)) + 1);
	}
}
