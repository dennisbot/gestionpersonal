package com.lasbambas.gestionpersonal.dao.nivelJerarquico;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;
import com.lasbambas.gestionpersonal.model.NivelJerarquico;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;

@Repository
public class NivelJerarquicoDaoImpl implements NivelJerarquicoDao{

	@PersistenceContext
	EntityManager em;

	public List<NivelJerarquico> findAll() {
		String sqlQuery = "SELECT n FROM NivelJerarquico n";
		TypedQuery<NivelJerarquico> query = em.createQuery(sqlQuery, NivelJerarquico.class);
		return query.getResultList();
	}

	public NivelJerarquico findById(int idNivelJerarquico) {
		try {
//			String sqlQuery = "SELECT n FROM NivelJerarquico n " +
//					"JOIN FETCH n.jerarquiaOrganizacionals " +
//					"WHERE n.idNivelJerarquico=:idNivelJerarquico";
//			TypedQuery<NivelJerarquico> query = em.createQuery(sqlQuery, NivelJerarquico.class);
//			query.setParameter("idNivelJerarquico", idNivelJerarquico);
//			return query.getSingleResult();

//			NivelJerarquico nj = query.getSingleResult();
//			int sizeJerarquiasOrgs = nj.getJerarquiaOrganizacionals().size();
//			System.out.println("sizeJerarquiasOrgs:");
//			System.out.println(sizeJerarquiasOrgs);
//			HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//			for (JerarquiaOrganizacional jerarquia : nj.getJerarquiaOrganizacionals()) {
//				System.out.println(nj.getDescripcionNivelJerarquico());
//				System.out.println(mapper.writeValueAsString(jerarquia));
//			}
//			return null;
//			return query.getSingleResult();

			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<NivelJerarquico> criteria = cb.createQuery(NivelJerarquico.class);
			Root<NivelJerarquico> nivelJerarquico = criteria.from(NivelJerarquico.class);
			nivelJerarquico.fetch("jerarquiaOrganizacionals");
			criteria.select(nivelJerarquico).where(
					cb.equal(nivelJerarquico.get("idNivelJerarquico"), idNivelJerarquico)
			);
//			NivelJerarquico h = em.createQuery(criteria).getSingleResult();
//			HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//			String val = mapper.writeValueAsString(h);
//			System.out.println("NivelJerarquico:");
//			System.out.println(val);
//
//			return h;
			return em.createQuery(criteria).getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró el nivel jerarquico con el id = "
					+ idNivelJerarquico);
			return null;
		}
	}
	public NivelJerarquico findByNivel(int nivel) {
		try {
			String sqlQuery = "SELECT n FROM NivelJerarquico n " +
					"WHERE n.nivel=:nivel";
			TypedQuery<NivelJerarquico> query = em.createQuery(sqlQuery, NivelJerarquico.class);
			query.setParameter("nivel", nivel);
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("no se encontró el nivel jerarquico con el nivel = "
					+ nivel);
			return null;
		}
	}

	public List<NivelJerarquico> getLessThanEqualNivel(int nivel) {
		try {
			String sqlQuery = "SELECT n FROM NivelJerarquico n " +
					"WHERE n.nivel <= :nivel";
			TypedQuery<NivelJerarquico> query = em.createQuery(sqlQuery, NivelJerarquico.class);
			query.setParameter("nivel", nivel);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró el nivel jerarquico con el nivel <= "
					+ nivel);
			return null;
		}
	}


	public NivelJerarquico save(NivelJerarquico nivelJerarquico) {
		// TODO Auto-generated method stub
		return null;
	}

	public NivelJerarquico create(NivelJerarquico nivelJerarquico) {
		return null;
	}

	public NivelJerarquico update(NivelJerarquico nivelJerarquico) {
		return em.merge(nivelJerarquico);
	}

	public boolean remove(int idNivelJerarquico) {
		// TODO Auto-generated method stub
		return false;
	}
}
