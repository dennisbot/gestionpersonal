package com.lasbambas.gestionpersonal.dao.rol;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.projection.RolWrapperSelect;

public interface RolDao {
	public List<RolWrapperSelect> findAll();
	public RolWrapperSelect findById(int idRol);
}
