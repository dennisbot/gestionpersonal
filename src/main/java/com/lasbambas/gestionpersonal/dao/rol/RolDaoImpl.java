package com.lasbambas.gestionpersonal.dao.rol;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;
import com.lasbambas.gestionpersonal.model.projection.RolWrapperSelect;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;
import com.lasbambas.gestionpersonal.utils.queryUtil;

@Repository
public class RolDaoImpl implements RolDao{

	@PersistenceContext
	EntityManager em;

	public List<RolWrapperSelect> findAll() {
		String s = "SELECT ID_PARAMETRO_DETALLE AS ID_ROL, NOMBRE AS NOMBRE_ROL "
				+ "FROM PESTRATEGICO2.PARAMETRO_DETALLE "
				+ "WHERE ID_PARAMETRO = PORTAL.PESTRATEGICO2.FN_GetIDParametroByName('PESTRATEGICO_ROL')";
		Query query = em.createNativeQuery(s);
		return queryUtil.getResultList(query, RolWrapperSelect.class);
	}

	public RolWrapperSelect findById(int idRol) {
		String s = "SELECT ID_PARAMETRO_DETALLE AS ID_ROL, NOMBRE AS NOMBRE_ROL "
				+ "FROM PESTRATEGICO2.PARAMETRO_DETALLE "
				+ "WHERE ID_PARAMETRO = PORTAL.PESTRATEGICO2.FN_GetIDParametroByName('PESTRATEGICO_ROL') "
				+ "WHERE ID_PARAMETRO_DETALLE = ?";
		Query query = em.createNativeQuery(s);
		query.setParameter(0, idRol);
		return queryUtil.getResultList(query, RolWrapperSelect.class).get(0);
	}
}