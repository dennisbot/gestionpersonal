package com.lasbambas.gestionpersonal.dao.empleado;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;
import com.lasbambas.gestionpersonal.utils.MiddlewareUtil;
import com.lasbambas.gestionpersonal.utils.queryUtil;

@Repository
public class EmpleadoDaoImpl implements EmpleadoDao {

	@PersistenceContext
	private EntityManager em;

	public Empleado canLogIn(Empleado empleado) throws Exception {
		empleado.setPassword(MiddlewareUtil.encriptaPassword(empleado.getPassword()));
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Empleado> criteria = cb.createQuery(Empleado.class);
		Root<Empleado> e = criteria.from(Empleado.class);
		e.fetch("cargo", JoinType.LEFT);
		criteria.select(e)
		.where(
				cb.equal(e.get("usuario"), empleado.getUsuario()),
				cb.equal(e.get("password"), empleado.getPassword())
		);
		try {
			return em.createQuery(criteria).getSingleResult();
		} catch (Exception ex) {
			return null;
		}
	}
	public List<Empleado> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Empleado> criteria = cb.createQuery(Empleado.class);
		Root<Empleado> empleado = criteria.from(Empleado.class);
		empleado.fetch("cargo", JoinType.LEFT);
		criteria.select(empleado);
		return em.createQuery(criteria).getResultList();
	}
	public Empleado findById(int idEmpleado) {
		return em.find(Empleado.class, idEmpleado);
	}
	public List<EmpleadoWrapperSelect> toSelect() {
		// esta consulta funciona a partir de la versión 3.6.8 (es un bug previo) 
//		String s = "select new "
//				+ "com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect"
//				+ "(e.idEmpleado,"
//				+ "e.nombres,"
//				+ "e.paterno,"
//				+ "e.materno,"
//				+ "e.uuidFoto,"
//				+ "c.descripcionCargo"
//				+ ") from Empleado e left join e.cargo c";
//		TypedQuery<EmpleadoWrapperSelect> query = em.createQuery(s, EmpleadoWrapperSelect.class);
//		return query.getResultList();
		
		String s = "SELECT E.ID_EMPLEADO, NOMBRES, PATERNO, MATERNO, "
				+ "COALESCE(UUID_FOTO, '') UUIDFOTO, "
				+ "COALESCE(C.ID_CARGO, 0) ID_CARGO, "
				+ "COALESCE(C.DESCRIPCION_CARGO, '') DESCRIPCIONCARGO, "
				+ "COALESCE(do.ID_DEPENDENCIA_ORGANIZACIONAL, 0) ID_DEPENDENCIA_ORGANIZACIONAL, "
				+ "COALESCE(do.ID_NIVEL_JERARQUICO, 0) ID_NIVEL_JERARQUICO "
				+ "FROM PORTAL.GPERSONAL.EMPLEADO E LEFT OUTER JOIN "
				+ "PORTAL.GPERSONAL.CARGO C "
				+ "ON E.ID_EMPLEADO = C.ID_EMPLEADO "
				+ "LEFT OUTER JOIN "
				+ "PORTAL.GPERSONAL.DEPENDENCIA_ORGANIZACIONAL do "
				+ "ON "
				+ "C.ID_DEPENDENCIA_ORGANIZACIONAL = do.ID_DEPENDENCIA_ORGANIZACIONAL";
		Query query = em.createNativeQuery(s);
		return queryUtil.getResultList(query, EmpleadoWrapperSelect.class);
		
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<EmpleadoWrapperSelect> criteria = cb.createQuery(EmpleadoWrapperSelect.class);
//		Root<Empleado> empleado = criteria.from(Empleado.class);
////		empleado.join("cargo", JoinType.LEFT);
//		empleado.fetch("cargo", JoinType.LEFT);	
//		criteria.multiselect(
//					empleado.get("idEmpleado"),
//					empleado.get("nombres"),
//					empleado.get("paterno"),
//					empleado.get("materno"),
//					empleado.get("uuidFoto"),
//					empleado.get("cargo").get("descripcionCargo")
//		);
//		return em.createQuery(criteria).getResultList();

	}

	public Empleado save(Empleado empleado) {
		// TODO Auto-generated method stub
		return null;
	}

	public Empleado create(Empleado empleado) throws Exception {
		empleado.setPassword(MiddlewareUtil.encriptaPassword("123456"));
		em.persist(empleado);
		em.flush();
		return empleado;
	}

	public Empleado update(Empleado empleado) {
		return em.merge(empleado);
	}
	public static void main(String[] args) throws Exception {
		// sacar el password como encriptado
		String password = MiddlewareUtil.encriptaPassword("123456");
		System.out.println("password:");
		System.out.println(password);
	}
}
