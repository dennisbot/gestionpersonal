package com.lasbambas.gestionpersonal.dao.empleado;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;

public interface EmpleadoDao {
	public Empleado canLogIn(Empleado empleado) throws Exception;
	public List<Empleado> findAll();
	public Empleado findById(int idEmpleado);
	public List<EmpleadoWrapperSelect> toSelect();
	public Empleado save(Empleado empleado);
	public Empleado create(Empleado empleado)  throws Exception;
	public Empleado update(Empleado empleado);
}
