package com.lasbambas.gestionpersonal.dao.cargo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;

@Repository
public class CargoDaoImpl implements CargoDao{

	@PersistenceContext
	EntityManager em;

	public List<Cargo> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Cargo> criteria = cb.createQuery(Cargo.class);
		Root<Cargo> cargo = criteria.from(Cargo.class);
//		cargo.fetch("dependenciaOrganizacional");
		cargo.fetch("empleado", JoinType.LEFT);
		criteria.select(cargo);
		return em.createQuery(criteria).getResultList();
	}

	public Cargo findById(int idCargo) {
//		return em.find(Cargo.class, idCargo);
		
		try {
			String sqlQuery = "SELECT c FROM Cargo c " +
					"JOIN FETCH c.dependenciaOrganizacional do  " +
					"JOIN FETCH do.nivelJerarquico n " + 
					"WHERE c.idCargo = :idCargo";
			TypedQuery<Cargo> query = em.createQuery(sqlQuery, Cargo.class);
			query.setParameter("idCargo", idCargo);
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("no se encontró la dependenciaOrganizacional con el idCargo = "
					+ idCargo);
			return null;
		}
		
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<Cargo> criteria = cb.createQuery(Cargo.class);
//		Root<Cargo> cargo = criteria.from(Cargo.class);
//		cargo.fetch("dependenciaOrganizacional");
//		cargo.fetch("empleado", JoinType.LEFT);
////		cargo.fetch("empleado");
//		criteria.select(cargo).where(cb.equal(cargo.get("idCargo"), idCargo));
//		return em.createQuery(criteria).getSingleResult();
	}
	
	public List<Cargo> findByIdDependencia(int idDependencia) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Cargo> criteria = cb.createQuery(Cargo.class);
		Root<Cargo> cargo = criteria.from(Cargo.class);
//		cargo.fetch("dependenciaOrganizacional");
		cargo.fetch("empleado", JoinType.LEFT);
		criteria.select(cargo)
				.where(
						cb.equal(
								cargo
									.get("dependenciaOrganizacional")
									.get("idDependenciaOrganizacional"), 
								idDependencia
						)
				);
		return em.createQuery(criteria).getResultList();
	}
	
	public Cargo findByNivel(int nivel) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Cargo> getLessThanEqualNivel(int nivel) {
		// TODO Auto-generated method stub
		return null;
	}

	public Cargo save(Cargo cargo) {
		// TODO Auto-generated method stub
		return null;
	}

	public Cargo create(Cargo cargo) {
		em.persist(cargo);
		em.flush();
		return cargo;
	}

	public Cargo update(Cargo cargo) {
		return em.merge(cargo);
	}

	public boolean remove(int idCargo) {
		Cargo c = em.find(Cargo.class, idCargo);
		em.remove(c);
		return true;
	}
}
