package com.lasbambas.gestionpersonal.dao.cargo;

import java.util.List;

import com.lasbambas.gestionpersonal.model.Cargo;

public interface CargoDao {
	public List<Cargo> findAll();
	public Cargo findById(int idCargo);
	public Cargo findByNivel(int nivel);
	public List<Cargo> findByIdDependencia(int idDependencia);
	public List<Cargo> getLessThanEqualNivel(int nivel);
	public Cargo save(Cargo cargo);
	public Cargo create(Cargo cargo);
	public Cargo update(Cargo cargo);
	public boolean remove(int idCargo);
}
