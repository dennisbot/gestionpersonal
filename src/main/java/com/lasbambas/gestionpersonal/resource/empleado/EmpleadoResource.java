package com.lasbambas.gestionpersonal.resource.empleado;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;

public interface EmpleadoResource {
	public List<Empleado> findAll();
	public Empleado findById(int idEmpleado);
	public List<EmpleadoWrapperSelect> toSelect();
	public Empleado save(Empleado empleado);
	public Empleado create(Empleado empleado) throws JsonProcessingException, Exception;
	public Empleado update(Empleado empleado);
	public Empleado remove(int idEmpleado);
}
