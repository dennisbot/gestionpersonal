/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lasbambas.gestionpersonal.resource.empleado;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lasbambas.gestionpersonal.model.Empleado;
import com.lasbambas.gestionpersonal.model.projection.EmpleadoWrapperSelect;
import com.lasbambas.gestionpersonal.model.projection.WrapperChangePassword;
import com.lasbambas.gestionpersonal.service.empleado.EmpleadoService;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;
import com.lasbambas.gestionpersonal.utils.MiddlewareUtil;

@Controller
@RequestMapping("/rest/empleado")
public class EmpleadoResourceImpl implements EmpleadoResource {
	@Autowired
	private EmpleadoService empleadoService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Empleado> findAll() {
		return empleadoService.findAll();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Empleado findById(@PathVariable("id") int idEmpleado) {
		return empleadoService.findById(idEmpleado);
	}

	@RequestMapping(value = "toselect", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<EmpleadoWrapperSelect> toSelect() {
		return empleadoService.toSelect();
	}

	public Empleado save(Empleado empleado) {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Empleado create(@RequestBody Empleado empleado) throws Exception {
		 HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
		 String val = mapper.writeValueAsString(empleado);
		 System.out.println("empleado as json:");
		 System.out.println(val);
		return empleadoService.create(empleado);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Empleado update(@RequestBody Empleado empleado) {
		return empleadoService.update(empleado);
	}

	@RequestMapping(value = "updateProfile/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Empleado updateProfile(@RequestBody Empleado empleado,
			HttpSession session) {
		session.setAttribute("usuario", empleado);
		return empleadoService.update(empleado);
	}

	public Empleado remove(int idEmpleado) {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, ? extends Object> changePassword(
			@RequestBody WrapperChangePassword wChangePassword)
			throws Exception {
		System.out.println("wChangePassword.oldPassword");
		System.out.println(wChangePassword.oldPassword);

		System.out.println("wChangePassword.newPassword");
		System.out.println(wChangePassword.newPassword);

		HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
		String val = mapper.writeValueAsString(wChangePassword.empleado);
		System.out.println("empleado:");
		System.out.println(val);

		Empleado empleado = wChangePassword.empleado, sameEmp;
		empleado.setPassword(wChangePassword.oldPassword);
		Map<String, Object> m = new HashMap<String, Object>();
		sameEmp = empleadoService.canLogIn(empleado);
		m.put("error", false);
		if (sameEmp != null) {
			empleado.setPassword(MiddlewareUtil
					.encriptaPassword(wChangePassword.newPassword));
			empleadoService.update(empleado);
			m.put("message", "contraseña cambiada exitosamente");
		} else {
			m.put("error", true);
			m.put("message", "la contraseña antigua no es correcta");
		}
		return m;

	}
}
