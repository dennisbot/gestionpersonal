/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lasbambas.gestionpersonal.resource.cargo;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.service.cargo.CargoService;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;

@Controller
@RequestMapping("/rest/cargo")
public class CargoResourceImpl implements CargoResource {
    @Autowired
    private CargoService cargoService;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public @ResponseBody List<Cargo> findAll() {
    	List<Cargo> ll = cargoService.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of cargo:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public @ResponseBody Cargo findById(@PathVariable("id") int idCargo) {
    	return cargoService.findById(idCargo);
    }

    @RequestMapping(value="dependencia/{id}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
    public @ResponseBody List<Cargo> findByIdDependencia(@PathVariable("id") int idDependencia) {
    	return cargoService.findByIdDependencia(idDependencia);
	}

    @RequestMapping(value="getNivel/{nivel}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
    public @ResponseBody Cargo findByNivel(@PathVariable("nivel") int nivel) {
    	return cargoService.findByNivel(nivel);
	}

    @RequestMapping(value="getLessThanEqualNivel/{nivel}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    		)
    public @ResponseBody List<Cargo> getLessThanEqualNivel(@PathVariable("nivel") int nivel) {
    	return cargoService.getLessThanEqualNivel(nivel);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody Cargo create(@RequestBody Cargo cargo) {
		return cargoService.create(cargo);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody Cargo update(@RequestBody Cargo cargo) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(cargo);
//			 System.out.println("cargo:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cargoService.update(cargo);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody boolean remove(@PathVariable("id") int idCargo) {
		return cargoService.remove(idCargo);
    }
}
