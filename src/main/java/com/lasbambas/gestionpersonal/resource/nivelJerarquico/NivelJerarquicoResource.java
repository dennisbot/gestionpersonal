package com.lasbambas.gestionpersonal.resource.nivelJerarquico;

import java.util.List;

import com.lasbambas.gestionpersonal.model.NivelJerarquico;

public interface NivelJerarquicoResource {
	public List<NivelJerarquico> findAll();
	public NivelJerarquico findById(int idNivelJerarquico);
	public NivelJerarquico findByNivel(int nivel);
	public List<NivelJerarquico> getLessThanEqualNivel(int nivel);
	public NivelJerarquico create(NivelJerarquico nivelJerarquico);
	public NivelJerarquico update(NivelJerarquico nivelJerarquico);
	public boolean remove(int idNivelJerarquico);
}
