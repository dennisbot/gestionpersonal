/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lasbambas.gestionpersonal.resource.nivelJerarquico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lasbambas.gestionpersonal.model.NivelJerarquico;
import com.lasbambas.gestionpersonal.service.nivelJerarquico.NivelJerarquicoService;
import com.lasbambas.gestionpersonal.utils.HibernateAwareObjectMapper;

@Controller
@RequestMapping("/rest/nivelJerarquico")
public class NivelJerarquicoResourceImpl implements NivelJerarquicoResource {
    @Autowired
    private NivelJerarquicoService nivelJerarquicoService;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public @ResponseBody List<NivelJerarquico> findAll() {
    	List<NivelJerarquico> ll = nivelJerarquicoService.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of nivelJerarquico:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public @ResponseBody NivelJerarquico findById(@PathVariable("id") int idNivelJerarquico) {
    	return nivelJerarquicoService.findById(idNivelJerarquico);
    }

    @RequestMapping(value="getNivel/{nivel}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
    public @ResponseBody NivelJerarquico findByNivel(@PathVariable("nivel") int nivel) {
    	return nivelJerarquicoService.findByNivel(nivel);
	}

    @RequestMapping(value="getLessThanEqualNivel/{nivel}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    		)
    public @ResponseBody List<NivelJerarquico> getLessThanEqualNivel(@PathVariable("nivel") int nivel) {
    	return nivelJerarquicoService.getLessThanEqualNivel(nivel);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody NivelJerarquico create(@RequestBody NivelJerarquico nivelJerarquico) {
		return nivelJerarquicoService.create(nivelJerarquico);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody NivelJerarquico update(@RequestBody NivelJerarquico nivelJerarquico) {
    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			val = mapper.writeValueAsString(nivelJerarquico);
			System.out.println("nivelJerarquico:");
	    	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nivelJerarquicoService.update(nivelJerarquico);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public @ResponseBody boolean remove(@PathVariable("id") int idNivelJerarquico) {
		return nivelJerarquicoService.remove(idNivelJerarquico);
	}
}
