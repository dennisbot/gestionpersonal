/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lasbambas.gestionpersonal.resource;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lasbambas.gestionpersonal.dao.cargo.CargoDao;
import com.lasbambas.gestionpersonal.dao.dependenciaOrganizacional.DependenciaOrganizacionalDao;
import com.lasbambas.gestionpersonal.dao.empleado.EmpleadoDao;
import com.lasbambas.gestionpersonal.dao.nivelJerarquico.NivelJerarquicoDao;
import com.lasbambas.gestionpersonal.dao.rol.RolDao;
import com.lasbambas.gestionpersonal.model.Empleado;

@Controller
@RequestMapping("/rest/parametros")
public class ParametrosResource {
    @Autowired
    private EmpleadoDao empleadoDao;
    @Autowired
    private NivelJerarquicoDao nivelJerarquicoDao;
    @Autowired
    private DependenciaOrganizacionalDao dependenciaOrganizacionalDao;
    @Autowired
    private CargoDao cargoDao;
    @Autowired
    private RolDao rolDao;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> parametros(HttpSession session) {
    	HashMap<String, Object> mapa = new HashMap<String, Object>();
    	Empleado usuario = (Empleado)session.getAttribute("usuario");
    	mapa.put("usuario", usuario);
    	return mapa;
    }

    @RequestMapping(value="dependenciaOrganizacionalViewParams/{nivel}/{idDependencia}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> dependenciaOrganizacionalViewParams(@PathVariable("nivel") int nivel,
    		@PathVariable("idDependencia") int idDependencia) {
    	HashMap<String, Object> mapa = new HashMap<String, Object>();
    	mapa.put("nivelesJerarquicos", nivelJerarquicoDao.getLessThanEqualNivel(nivel));
    	mapa.put("allNivelesJerarquicos", nivelJerarquicoDao.findAll());
    	mapa.put("dependencias", dependenciaOrganizacionalDao.findAll());
    	mapa.put("dependenciasOrgsLevel", dependenciaOrganizacionalDao.findByNivel(nivel, idDependencia));
    	mapa.put("oneLevelBackDeps", dependenciaOrganizacionalDao.findOneLevelBackDeps(nivel));
    	mapa.put("hierachyDep", dependenciaOrganizacionalDao.getHierachy(idDependencia));
    	return mapa;
    }

    @RequestMapping(value="cargoViewParams/{nivel}/{idDependencia}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> cargoViewParams(
    		@PathVariable("nivel") int nivel,
    		@PathVariable("idDependencia") int idDependencia) {
    	// System.out.println("idDependencia:");
    	// System.out.println(idDependencia);
    	HashMap<String, Object> mapa = new HashMap<String, Object>();
    	mapa.put("cargos", cargoDao.findByIdDependencia(idDependencia));
    	mapa.put("empleados", empleadoDao.toSelect());
    	mapa.put("nivelesJerarquicos", nivelJerarquicoDao.getLessThanEqualNivel(nivel));
    	mapa.put("hierachyDep", dependenciaOrganizacionalDao.getHierachy(idDependencia));
    	return mapa;
    }

    @RequestMapping(value="empleadoViewParams", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> empleadoViewParams() {
    	HashMap<String, Object> mapa = new HashMap<String, Object>();
    	mapa.put("empleados", empleadoDao.toSelect());
    	mapa.put("roles", rolDao.findAll());
    	return mapa;
    }

    @RequestMapping(value="rol", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> rol() {
    	HashMap<String, Object> mapa = new HashMap<String, Object>();
    	mapa.put("roles", rolDao.findAll());
    	return mapa;
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
//    public @ResponseBody Member lookupMemberById(@PathVariable("id") Long id) {
//        return memberDao.findById(id);
//    }
}
