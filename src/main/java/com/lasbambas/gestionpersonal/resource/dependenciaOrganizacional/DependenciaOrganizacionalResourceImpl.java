package com.lasbambas.gestionpersonal.resource.dependenciaOrganizacional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;
import com.lasbambas.gestionpersonal.service.dependenciaOrganizacional.DependenciaOrganizacionalService;
@Controller
@RequestMapping("rest/dependenciaOrganizacional")
public class DependenciaOrganizacionalResourceImpl implements DependenciaOrganizacionalResource {
	@Autowired
	DependenciaOrganizacionalService dependenciaOrganizacionalService;

	@RequestMapping(
			method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	public @ResponseBody List<DependenciaOrganizacional> findAll() {
		return dependenciaOrganizacionalService.findAll();
	}
	@RequestMapping(
			value="{id}",
			method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	public @ResponseBody DependenciaOrganizacional findById(@PathVariable("id") int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalService.findById(idDependenciaOrganizacional);
	}
	@RequestMapping(
			value="getByNivel/{nivel}",
			method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	public @ResponseBody List<DependenciaOrganizacional> findByNivel(@PathVariable("nivel") int nivel) {
		return dependenciaOrganizacionalService.findByNivel(nivel, 0);
	}
	
	@RequestMapping(
			value="getHierachy/{idDependenciaOrganizacional}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public @ResponseBody List<DependenciaOrganizacional> getHierachy(
			@PathVariable("idDependenciaOrganizacional") int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalService.getHierachy(idDependenciaOrganizacional);
	}
	
	@RequestMapping(
			value="existeCodDependencia/{codDependencia}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public @ResponseBody boolean existeCodDependencia(
			@PathVariable("codDependencia") String codDependencia) {
		return dependenciaOrganizacionalService.existeCodDependencia(codDependencia);
	}
	
	@RequestMapping(
			value="findOneLevelBackDeps/{nivel}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public @ResponseBody List<DependenciaOrganizacional> findOneLevelBackDeps(
			@PathVariable("nivel") int nivel) {
		return dependenciaOrganizacionalService.findOneLevelBackDeps(nivel);
	}
	
	@RequestMapping(
			value="findDepsBetweenLevels/{loLvl}/{hiLvl}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public @ResponseBody List<DependenciaOrganizacional> findDepsBetweenLevels(
			@PathVariable("loLvl") Integer loLvl, @PathVariable("hiLvl") Integer hiLvl) {
		return dependenciaOrganizacionalService.findDepsBetweenLevels(loLvl, hiLvl);
	}
	@RequestMapping(
			value="nextCodDependencia",
			method = RequestMethod.GET,
//			produces = MediaType.TEXT_PLAIN_VALUE
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	public @ResponseBody String nextCodDependencia() {
		return dependenciaOrganizacionalService.nextCodDependencia();
	}
	
	@RequestMapping(
			method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	public @ResponseBody DependenciaOrganizacional create(
			@RequestBody DependenciaOrganizacional dependenciaOrganizacional) {
		return dependenciaOrganizacionalService.create(dependenciaOrganizacional);
	}
	@RequestMapping(
			value="{id}",
			method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody DependenciaOrganizacional update(
		   @RequestBody	DependenciaOrganizacional dependenciaOrganizacional) {
		return dependenciaOrganizacionalService.update(dependenciaOrganizacional);
	}
	@RequestMapping(
			value="{id}",
			method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	public @ResponseBody boolean remove(@PathVariable("id") int idDependenciaOrganizacional) {
		return dependenciaOrganizacionalService.remove(idDependenciaOrganizacional);
	}

}
