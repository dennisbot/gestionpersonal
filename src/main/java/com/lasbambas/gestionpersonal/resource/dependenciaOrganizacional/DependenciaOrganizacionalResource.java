package com.lasbambas.gestionpersonal.resource.dependenciaOrganizacional;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;

public interface DependenciaOrganizacionalResource {
	public List<DependenciaOrganizacional> findAll();
	public DependenciaOrganizacional findById(int idDependenciaOrganizacional);
	public List<DependenciaOrganizacional> findByNivel(int nivel);
	public List<DependenciaOrganizacional> getHierachy(int idDependenciaOrganizacional);
	public DependenciaOrganizacional create(DependenciaOrganizacional dependenciaOrganizacional);
	public DependenciaOrganizacional update(DependenciaOrganizacional dependenciaOrganizacional);
	public boolean remove(int idDependenciaOrganizacional);
}
