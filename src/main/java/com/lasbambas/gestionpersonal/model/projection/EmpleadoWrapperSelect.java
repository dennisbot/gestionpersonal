package com.lasbambas.gestionpersonal.model.projection;

import java.io.Serializable;

import org.apache.commons.lang.WordUtils;

import com.lasbambas.gestionpersonal.model.Cargo;
import com.lasbambas.gestionpersonal.model.DependenciaOrganizacional;
import com.lasbambas.gestionpersonal.model.NivelJerarquico;

public class EmpleadoWrapperSelect implements Serializable {
	private Integer idEmpleado;
	private String nombres;
	private String paterno;
	private String materno;
	private String uuidFoto;
	private Cargo cargo;
	
	public EmpleadoWrapperSelect() {
	}
	public EmpleadoWrapperSelect(Integer idEmpleado, String nombres,
			String paterno, String materno, String uuidFoto,
			Integer idCargo,
			String descripcionCargo,
			Integer idDependenciaOrganizacional, 
			Integer idNivelJerarquico) {
		super();
		this.idEmpleado = idEmpleado;
		this.nombres = nombres;
		this.paterno = paterno;
		this.materno = materno;
		this.uuidFoto = uuidFoto.equals("") ? null : uuidFoto;
		if (idCargo != 0) {
			this.cargo = new Cargo();
			this.cargo.setIdCargo(idCargo);
			this.cargo.setDescripcionCargo(descripcionCargo.equals("") ? "(por asignar)" : descripcionCargo);
			if (idDependenciaOrganizacional != 0) {
				DependenciaOrganizacional depOrg = new DependenciaOrganizacional();
				depOrg = new DependenciaOrganizacional();
				depOrg.setIdDependenciaOrganizacional(idDependenciaOrganizacional);
				NivelJerarquico nj = new NivelJerarquico();
				nj.setIdNivelJerarquico(idNivelJerarquico);
				depOrg.setNivelJerarquico(nj);
				this.cargo.setDependenciaOrganizacional(depOrg);
			}
		}
		
	}
	public int getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getNombreParaMostrar() {		
		return WordUtils.capitalizeFully(this.nombres + " " + this.paterno);
	}
	public String getUuidFoto() {
		return uuidFoto;
	}
	public void setUuidFoto(String uuidFoto) {
		this.uuidFoto = uuidFoto;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
