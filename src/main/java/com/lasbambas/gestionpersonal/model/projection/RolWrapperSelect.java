package com.lasbambas.gestionpersonal.model.projection;

import java.io.Serializable;

public class RolWrapperSelect implements Serializable {
	private Integer idRol;
	private String descripcion;
	public RolWrapperSelect(Integer idRol, String descripcion) {
		super();
		this.idRol = idRol;
		this.descripcion = descripcion;
	}
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}