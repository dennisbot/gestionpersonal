package com.lasbambas.gestionpersonal.model.projection;

import java.io.Serializable;

import com.lasbambas.gestionpersonal.model.Empleado;

public class WrapperChangePassword implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Empleado empleado;
	public String oldPassword;
	public String newPassword;
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}	
}
